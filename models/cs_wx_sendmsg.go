package models

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"github.com/astaxie/beego/orm"
)

type CsWxSendmsg struct {
	Id       int    `orm:"column(id);auto"`
	Wxappid  string `orm:"column(wxappid);size(20);null"`
	Msgid    string `orm:"column(msgid);size(255);null"`
	SendTime string `orm:"column(send_time);type(datetime);null"`
	SendType int    `orm:"column(send_type);null"` // 消息类型 1：文字 2：图文
	TaskName string `orm:"column(task_name);size(255);null"`
}

func (t *CsWxSendmsg) TableName() string {
	return "cs_wx_sendmsg"
}

func init() {
	orm.RegisterModel(new(CsWxSendmsg))
}

// AddCsWxSendmsg insert a new CsWxSendmsg into database and returns
// last inserted Id on success.
func AddCsWxSendmsg(m *CsWxSendmsg) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetCsWxSendmsgById retrieves CsWxSendmsg by Id. Returns error if
// Id doesn't exist
func GetCsWxSendmsgById(id int) (v *CsWxSendmsg, err error) {
	o := orm.NewOrm()
	v = &CsWxSendmsg{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// get all cs send msg with time
func GetAllWxSendMsgByTime(nowtTime string) (sendList []CsWxSendmsg, err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_sendmsg").Filter("send_time__icontains", nowtTime).All(&sendList)
	if err != nil {
		fmt.Println("get all send error->", err.Error())
		return nil, err
	} else {
		return sendList, nil
	}

}

// Get all cs wx send msg
func GetAllCsWxSendMsgWithNoParam() (sendList []CsWxSendmsg, err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_sendmsg").All(&sendList)
	if err != nil {
		fmt.Println("get all send msg error->", err.Error())
		return nil, err
	} else {
		return sendList, nil
	}
}

// GetAllCsWxSendmsg retrieves all CsWxSendmsg matches certain condition. Returns empty list if
// no records exist
func GetAllCsWxSendmsg(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(CsWxSendmsg))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		if strings.Contains(k, "isnull") {
			qs = qs.Filter(k, (v == "true" || v == "1"))
		} else {
			qs = qs.Filter(k, v)
		}
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []CsWxSendmsg
	qs = qs.OrderBy(sortFields...)
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateCsWxSendmsg updates CsWxSendmsg by Id and returns error if
// the record to be updated doesn't exist
func UpdateCsWxSendmsgById(m *CsWxSendmsg) (err error) {
	o := orm.NewOrm()
	v := CsWxSendmsg{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteCsWxSendmsg deletes CsWxSendmsg by Id and returns error if
// the record to be deleted doesn't exist
func DeleteCsWxSendmsg(id int) (err error) {
	o := orm.NewOrm()
	v := CsWxSendmsg{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&CsWxSendmsg{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
