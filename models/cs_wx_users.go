package models

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"time"

	"github.com/astaxie/beego/orm"
)

// 关注用户信息表
type CsWxUsers struct {
	Id              int       `orm:"column(id);auto"`
	Sex             int       `orm:"column(sex);size(11);null"`                    // 性别 1：男 2：女
	Openid          string    `orm:"column(openid);size(255);null"`                // 用户ID
	Nickname        string    `orm:"column(nickname);size(11);null"`               // 用户昵称
	Language        string    `orm:"column(language);size(255);null"`              // 语言
	City            string    `orm:"column(city);size(255);null"`                  // 城市
	Province        string    `orm:"column(province);size(255);null"`              // 省份
	Country         string    `orm:"column(country);size(255);null"`               // 国家
	HeadImg         string    `orm:"column(headImg);size(255);null"`               // 头像
	Remark          string    `orm:"column(remark);size(255);null"`                // remark
	AppId           string    `orm:"column(appid);size(255);null"`                 // appid
	CreateTime      time.Time `orm:"column(create_time);type(datetime);null"`      // 首次关注时间
	SubScribeTime   time.Time `orm:"column(subscribe_time);type(datetime);null"`   // 最近一次关注时间
	UnSubScribeTime time.Time `orm:"column(unsubscribe_time);type(datetime);null"` // 最近一次取关时间
	LastMutualTime  time.Time `orm:"column(last_mutual_time);type(datetime);null"` // 最后一次交互时间
	SubScribeStatus int       `orm:"column(subscribe_status);size(3);null"`        // 关注状态 0：关注 1：已取关
}

// get all user with time and appid
func GetAllUserWithTimeAndAppid(appid string, startTime string) (userList []CsWxUsers, err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_users").Filter("appid", appid).Filter("subscribe_status", 0).Filter("last_mutual_time__gte", startTime).All(&userList)
	if err != nil {
		fmt.Println("get all user error->", err.Error())
		return nil, err
	} else {
		return userList, nil
	}
}

func (t *CsWxUsers) TableName() string {
	return "cs_wx_users"
}
func init() {
	orm.RegisterModel(new(CsWxUsers))
}

// add User to db
func AddWxUsers(model *CsWxUsers) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(model)
	return
}

// update wxuser with id
func UpdateWXUserWithId(model *CsWxUsers) (err error) {
	o := orm.NewOrm()
	v := CsWxAccount{Id: model.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		_, err = o.Update(model)

	}
	return
}

// add Users to db
func AddUsersTodb(models []CsWxUsers, InsertNum int) (succNum int64, err error) {
	o := orm.NewOrm()
	//	InsertUser := make([]CsWxUsers, 0)
	fmt.Println("time11111->", time.Now())
	//	for _, v := range models {
	//		u, _ := GetWxUserWithId(v.Openid)
	//		if u != nil {
	//			InsertNum--
	//		} else {
	//			InsertUser = append(InsertUser, v)
	//		}
	//		fmt.Println("time222222->", time.Now())
	//	}
	successNums, err := o.InsertMulti(InsertNum, models)
	fmt.Println("time33333->", time.Now())
	if err != nil {
		for _, v := range models {
			_, errIns := AddWxUsers(&v)
			if errIns != nil {
				defStr, _ := json.Marshal(v)
				fmt.Println("list->", string(defStr))
				md5Ctx := md5.New()
				md5Ctx.Write([]byte(v.Nickname))
				v.Nickname = hex.EncodeToString(md5Ctx.Sum(nil))
				_, _ = AddWxUsers(&v)
			}
		}
		return successNums, err
	} else {
		return successNums, nil
	}
}

// get wxUserNum with appid
func GetUserNumWithAppid(appid string) (num int, err error) {
	o := orm.NewOrm()
	Num, err := o.QueryTable("cs_wx_users").Filter("appid", appid).Count()
	if err != nil {
		return 0, err
	} else {
		return int(Num), nil
	}
}

// get user with openId
func GetWxUserWithId(id string) (v *CsWxUsers, err error) {
	o := orm.NewOrm()
	v = &CsWxUsers{Openid: id}
	if err = o.Read(v, "openid"); err == nil {
		return v, nil
	}
	return nil, err
}

// get all user
func GetAllwxUser() (users []CsWxUsers, b bool) {
	o := orm.NewOrm()
	users = make([]CsWxUsers, 0)
	o.QueryTable("cs_wx_users").All(&users)
	return users, true
}

// get all user with appid
func GetAllWXUserWithAppId(appid string) (users []CsWxUsers, b bool) {
	o := orm.NewOrm()
	users = make([]CsWxUsers, 0)
	o.QueryTable("cs_wx_users").Filter("appid", appid).All(&users)
	return users, true
}

// get  all user with two appid
func GetAllWXUserWithTwoAppID(appid_one, appid_two string) (users []CsWxUsers, b bool) {
	o := orm.NewOrm()
	users_one := make([]CsWxUsers, 0)
	users_two := make([]CsWxUsers, 0)
	o.QueryTable("cs_wx_users").Filter("appid", appid_one).All(&users_one)
	o.QueryTable("cs_wx_users").Filter("appid", appid_two).All(&users_two)
	for _, user := range users_two {
		users_one = append(users_one, user)
	}
	return users_one, true
}

// get  user with openid
func GetWXUserWithOpenId(openid string) (user *CsWxUsers, err error) {
	o := orm.NewOrm()
	v := &CsWxUsers{Openid: openid}
	if err = o.Read(v, "openid"); err == nil {
		return v, nil
	}
	return nil, err
}

// get last openid with appid
func GetLastOpenIdWithAppid(appid string) (openId string) {
	seleSql := "SELECT * from cs_wx_users WHERE appid = '%s' GROUP BY id DESC LIMIT 10"
	o := orm.NewOrm()
	users := make([]CsWxUsers, 0)
	seleSqlStr := fmt.Sprintf(seleSql, appid)
	fmt.Println("sql->", seleSqlStr)
	_, err := o.Raw(seleSqlStr).QueryRows(&users)
	if err != nil {
		fmt.Println("err->", err.Error())
		return ""
	} else {
		for i := 0; i < len(users); i++ {
			if users[i].Openid != "" {
				return users[i].Openid
			}
		}
	}
	return ""
}
