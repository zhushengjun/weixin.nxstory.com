package models

import (
	"fmt"

	"github.com/astaxie/beego/orm"
)

// 发送模版任务表
type CsTaskStatus struct {
	Id           int `orm:"column(id);auto"`
	TaskId       int `orm:"column(task_id);size(11);0"`        //任务ID 和Task表中的ID对应
	SendStatus   int `orm:"column(send_status);size(11);0"`    //发送状态(0:未发送 1:发送中 2: 发送完成)
	SendTotalNum int `orm:"column(send_total_num);size(11);0"` //要发送的所有人总数
	SendSuccNum  int `orm:"column(send_succ_num);size(11);0"`  //发送成功的人的ID集合
}

func (t *CsTaskStatus) TableName() string {
	return "cs_task_status"
}

func init() {
	orm.RegisterModel(new(CsTaskStatus))
}

// add Task to db
func AddTaskStatus(task *CsTaskStatus) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(task)
	return
}

// get Task with id
func GetTaskStatusById(id int) (v *CsTaskStatus, err error) {
	o := orm.NewOrm()
	v = &CsTaskStatus{Id: id}
	if err = o.Read(v, "id"); err == nil {
		return v, nil
	}
	return nil, err
}
func GetTaskStatusByTaskId(taskId int) (v *CsTaskStatus, err error) {
	o := orm.NewOrm()
	v = &CsTaskStatus{TaskId: taskId}
	if err = o.Read(v, "task_id"); err == nil {
		return v, nil
	}
	return nil, err
}

// delete Task with id
func DeleteTaskStatusById(task *CsTaskStatus) (err error) {
	o := orm.NewOrm()
	v := &CsTaskStatus{Id: task.Id}
	if err = o.Read(v, "id"); err == nil {
		_, err = o.Delete(task)
	}
	return
}

// update Task with id
func UpdateTaskStatusById(task *CsTaskStatus) (err error) {
	o := orm.NewOrm()
	v := CsModelTask{Id: task.Id}
	if err = o.Read(&v); err == nil {
		_, err = o.Update(task)
	}
	return
}

// update task status and num with taskid
func UpdateTaskStatusAndNumById(taskId int, status int, num int) {
	o := orm.NewOrm()
	sql := fmt.Sprintf("update cs_task_status set send_status = %s,send_succ_num = %s where task_id = %s", status, num, taskId)
	o.Raw(sql).Exec()
}
