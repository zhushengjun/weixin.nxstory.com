package models

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"github.com/astaxie/beego/orm"
)

type CsWxAccount struct {
	Id         int    `orm:"column(id);auto"`
	WxAppId    string `orm:"column(wxappid);size(50);null"`
	Name       string `orm:"column(name);size(40);null"`
	Mail       string `orm:"column(mail);size(40);null"`
	Mtype      string `orm:"column(mtype);size(20);null"` // 1 服务号 2订阅号
	AppSecret  string `orm:"column(appsecret);size(40);null"`
	OriginalId string `orm:"column(origId);size(40);null"` // 原始ID
}

func (t *CsWxAccount) TableName() string {
	return "cs_wx_account"
}

func init() {
	orm.RegisterModel(new(CsWxAccount))
}

// GetCsGroupById retrieves CsGroup by Id. Returns error if
// Id doesn't exist
func GetCsWxAccountByAppId(appid string) (v *CsWxAccount, err error) {
	o := orm.NewOrm()
	v = &CsWxAccount{WxAppId: appid}
	if err = o.Read(v, "WxAppId"); err == nil {
		return v, nil
	}
	return nil, err
}

// GetCsWxAccount By OriginalId
func GetCsWxAccountByOriginalId(originalId string) (v *CsWxAccount, err error) {
	o := orm.NewOrm()
	v = &CsWxAccount{OriginalId: originalId}
	if err = o.Read(v, "origId"); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllCsMaterial retrieves all CsMaterial matches certain condition. Returns empty list if
// no records exist
func GetAllCsWxAccount(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(CsWxAccount))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		qs = qs.Filter(k, v)
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}
	var l []CsWxAccount
	qs = qs.OrderBy(sortFields...)
	if _, err := qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateCsGroup updates CsGroup by Id and returns error if
// the record to be updated doesn't exist
func UpdateCsWxAccountById(m *CsWxAccount) (err error) {
	o := orm.NewOrm()
	v := CsWxAccount{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		_, err = o.Update(m)

	}
	return
}

// get all wxAccount
func GetAllCsWeixinAccount() ([]CsWxAccount, error) {
	var accounts []CsWxAccount
	o := orm.NewOrm()
	_, err := o.QueryTable("cs_wx_account").All(&accounts)
	if err != nil {
		fmt.Println("err->", err.Error())
		return nil, err
	} else {
		return accounts, nil
	}
}

// get wxName with appid
func GetWxNameByAppid(appid string) (Name string, err error) {
	csWxAcc, err := GetCsWxAccountByAppId(appid)
	if err != nil {
		return "", err
	} else {
		return csWxAcc.Name, nil
	}
}

// get secret with appid
func GetAppSecretByAppid(appid string) (Secret string, err error) {
	csWxAcc, err := GetCsWxAccountByAppId(appid)
	if err != nil {
		return "", err
	} else {
		return csWxAcc.AppSecret, nil
	}
}
