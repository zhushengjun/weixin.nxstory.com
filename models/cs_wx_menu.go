package models

import (
	"errors"
	"reflect"
	"strings"

	"fmt"
	"time"

	"github.com/astaxie/beego/orm"
)

type CsWxMenu struct {
	Id         int       `orm:"column(id);auto"`
	WxAppId    string    `orm:"column(wxappid);size(50);null"`
	Pid        int       `orm:"column(pid);null"`
	OrderId    int       `orm:"column(orderid);1"`
	Name       string    `orm:"column(name);size(20);null"`
	Type       int       `orm:"column(type);1"`
	Tourl      string    `orm:"column(tourl);size(200);null"`
	MenuKey    string    `orm:"column(menukey);size(30);null"`
	ResContent string    `orm:"column(rescontent);size(30);null"`
	CreateTime time.Time `orm:"column(create_time);type(datetime);null"`
}

func (t *CsWxMenu) TableName() string {
	return "cs_wx_menu"
}

func init() {
	orm.RegisterModel(new(CsWxMenu))
}

func AddCsWxMenu(m *CsWxMenu) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

func GetMenuInfoWithKey(appid, key string) (v *CsWxMenu, err error) {
	o := orm.NewOrm()
	CsMenu := CsWxMenu{}
	o.Using("m_weixin")
	err = o.QueryTable("cs_wx_menu").Filter("wxappid", appid).Filter("menukey", key).One(&CsMenu)
	if err != nil {
		return nil, err
	} else {
		return &CsMenu, nil
	}
}

// Get menu with appid and pid
func GetMenuListWithAppidAndPid(appid string, pid int) (menuList []CsWxMenu, err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_menu").Filter("wxappid", appid).Filter("pid", pid).All(&menuList)
	if err != nil {
		fmt.Println("GetMenuListWithAppidAndPid err->", err.Error())
		return nil, err
	} else {
		return menuList, nil
	}
}

// GetCsGroupById retrieves CsGroup by Id. Returns error if
// Id doesn't exist
func GetCsWxMenuByAppId(appid string) (v *CsWxMenu, err error) {
	o := orm.NewOrm()
	o.Using("m_weixin")
	v = &CsWxMenu{WxAppId: appid}
	if err = o.Read(v, "WxAppId"); err == nil {
		return v, nil
	}
	return nil, err
}

func GetCsWxMenuById(id int) (v *CsWxMenu, err error) {
	o := orm.NewOrm()
	o.Using("m_weixin")
	v = &CsWxMenu{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// get all menu with appid
func GetAllMenuByAppid(appid string, pid int) (menulist []CsWxMenu, err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_menu").Filter("wxappid", appid).Filter("pid", pid).OrderBy("-create_time").All(&menulist)
	if err != nil {
		fmt.Println("GetAllMenuByAppid err->", err.Error())
		return nil, err
	} else {
		return menulist, nil
	}
}

// GetAllCsMaterial retrieves all CsMaterial matches certain condition. Returns empty list if
// no records exist
func GetAllCsWxMenu(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	o.Using("m_weixin")
	qs := o.QueryTable(new(CsWxMenu))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		qs = qs.Filter(k, v)
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}
	var l []CsWxMenu
	qs = qs.OrderBy(sortFields...)
	if _, err := qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateCsGroup updates CsGroup by Id and returns error if
// the record to be updated doesn't exist
func UpdateCsWxMenuById(m *CsWxMenu) (err error) {
	o := orm.NewOrm()
	o.Using("m_weixin")
	v := CsWxMenu{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		_, err = o.Update(m)

	}
	return
}

// deletet menu with pid
func DeleteMenuWithPid(pid int) (err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_menu").Filter("pid", pid).Delete()
	if err != nil {
		fmt.Println("DeleteMenuWithPid err->", err.Error())
	}
	return err
}
func DeleteCsWxMenu(id int) (err error) {
	o := orm.NewOrm()
	o.Using("m_weixin")
	v := CsWxMenu{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&CsWxMenu{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
