package models

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"reflect"
	"sort"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/cache"
	_ "github.com/astaxie/beego/cache/redis"
)

var mCache cache.Cache

func initCache() {
	if mCache == nil {
		redisconn := beego.AppConfig.String("cache::dataredis")
		redispass := beego.AppConfig.String("cache::dataredispass")
		redisdbnumber := beego.AppConfig.String("cache::dataredisdbnumber")
		if redisdbnumber == "" {
			redisdbnumber = "1"
		}
		fmt.Println(redisconn, redispass)
		mCache, _ = cache.NewCache("redis", `{"key":"redis","conn":"`+redisconn+`","dbNum":"`+redisdbnumber+`","password":"`+redispass+`"}`)
		fmt.Println("====redis cache create:", mCache)
	}
}
func PutCache(key string, value []byte, timeout int) {
	if beego.AppConfig.String("cache::usecache") != "true" {
		fmt.Println("disable use cache:", key)
		return
	}
	initCache()
	if mCache != nil {
		fmt.Println("PutCache key->", key)
		fmt.Println("PutCache value->", string(value))
		mCache.Put(key, value, 7000*time.Second)
	}
}
func DeleteCache(key string) {
	if beego.AppConfig.String("cache::usecache") != "true" {
		fmt.Println("disable use cache:", key)
		return
	}
	initCache()
	if mCache != nil {
		mCache.Delete(key)
	}
}
func GetCache(key string) []byte {
	if beego.AppConfig.String("cache::usecache") != "true" {
		fmt.Println("disable use cache:", key)
		return nil
	}
	initCache()
	value := mCache.Get(key)
	val, ok := value.([]byte)
	if ok {
		fmt.Println("GetCache key->", key)
		fmt.Println("GetCache value->", string(val))
		return val
	}
	return nil

}
func GetCacheStruct(key string) (ml []interface{}) {
	value := GetCache(key)
	if value == nil {
		return nil
	}
	var rs []interface{}
	json.Unmarshal(value, &rs)
	for _, v := range rs {
		m := make(map[string]interface{})
		val := v.(map[string]interface{})
		for k2, v2 := range val {
			reflectname := reflect.TypeOf(v2).Name()
			switch reflectname {
			case "float64":
				m[k2] = int(v2.(float64))
			case "string":
				m[k2] = v2
			default:
				fmt.Println("unknown type", reflectname, k2, v2)
			}
		}
		ml = append(ml, m)
	}

	return ml
}
func CacheMd5Key(str string) string {
	h := md5.New()
	h.Write([]byte(str))
	mret := hex.EncodeToString(h.Sum(nil))
	return mret
}
func CacheMapToString(data map[string]string) string {

	mreturn := ""

	sorted_keys := make([]string, 0)
	for k, _ := range data {
		sorted_keys = append(sorted_keys, k)
	}

	sort.Strings(sorted_keys)

	for _, k := range sorted_keys {
		mreturn += k
		mreturn += data[k]
	}

	return mreturn
}
func CacheStringsToString(data []string) string {
	mreturn := ""
	for _, v := range data {
		mreturn += v
	}
	return mreturn
}
