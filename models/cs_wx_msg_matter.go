package models

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"github.com/astaxie/beego/orm"
)

type CsWxMsgMatter struct {
	Id      int    `orm:"column(id);pk"`
	MsgName string `orm:"column(msg_name);size(255);null"` // 消息名称（用于自己区分）
	MsgType int    `orm:"column(msg_type);null"`           // 消息类型 1：文字 2：图文
	Content string `orm:"column(content);size(255);null"`  // 消息内容（type为1时有效）
	Title   string `orm:"column(title);size(255);null"`    // 图文标题（type为2时有效）
	Descrip string `orm:"column(descrip);size(255);null"`  // 图文详情（type为2时有效）
	Url     string `orm:"column(url);size(255);null"`      // 图文跳转（type为2时有效）
	Picurl  string `orm:"column(picurl);size(255);null"`   // 图文图片（type为2时有效）
}

func (t *CsWxMsgMatter) TableName() string {
	return "cs_wx_msg_matter"
}

func init() {
	orm.RegisterModel(new(CsWxMsgMatter))
}

// AddCsWxMsgMatter insert a new CsWxMsgMatter into database and returns
// last inserted Id on success.
func AddCsWxMsgMatter(m *CsWxMsgMatter) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetCsWxMsgMatterById retrieves CsWxMsgMatter by Id. Returns error if
// Id doesn't exist
func GetCsWxMsgMatterById(id int) (v *CsWxMsgMatter, err error) {
	o := orm.NewOrm()
	v = &CsWxMsgMatter{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// gei all cs weixin msg matter
func GetAllCsMsgMatterByNoParam(msgId int, msgName string, msgType int) (matterList []CsWxMsgMatter, err error) {
	o := orm.NewOrm()
	if msgId == 0 && msgName == "" && msgType == 0 { // 全部查找
		_, err = o.QueryTable("cs_wx_msg_matter").All(&matterList)
	}
	if msgId != 0 && msgName == "" && msgType == 0 { // 按照Id查询
		_, err = o.QueryTable("cs_wx_msg_matter").Filter("id", msgId).All(&matterList)
	}
	if msgId == 0 && msgName != "" && msgType == 0 { // 按照名字模糊查询
		_, err = o.QueryTable("cs_wx_msg_matter").Filter("msg_name__icontains", msgName).All(&matterList)
	}
	if msgId == 0 && msgName == "" && msgType != 0 { // 按照消息类型查询
		_, err = o.QueryTable("cs_wx_msg_matter").Filter("msg_type", msgType).All(&matterList)
	}
	if msgId != 0 && msgName != "" && msgType == 0 { // 按照Id查询 和名称查询
		_, err = o.QueryTable("cs_wx_msg_matter").Filter("id", msgId).Filter("msg_name__icontains", msgName).All(&matterList)
	}
	if msgId != 0 && msgName == "" && msgType != 0 { // 按照ID 类型查询
		_, err = o.QueryTable("cs_wx_msg_matter").Filter("id", msgId).Filter("msg_type", msgType).All(&matterList)
	}
	if msgId == 0 && msgName != "" && msgType != 0 { // 按照类型和名称模糊查询
		_, err = o.QueryTable("cs_wx_msg_matter").Filter("msg_name__icontains", msgName).Filter("msg_type", msgType).All(&matterList)
	}
	if err != nil {
		fmt.Println("get all msg matter error->", err.Error())
		fmt.Println(fmt.Sprintf("param:msgid:%d,msgname:%s,msgType:%d", msgId, msgName, msgType))
		return nil, err
	} else {
		return matterList, nil
	}
}

// GetAllCsWxMsgMatter retrieves all CsWxMsgMatter matches certain condition. Returns empty list if
// no records exist
func GetAllCsWxMsgMatter(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(CsWxMsgMatter))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		if strings.Contains(k, "isnull") {
			qs = qs.Filter(k, (v == "true" || v == "1"))
		} else {
			qs = qs.Filter(k, v)
		}
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []CsWxMsgMatter
	qs = qs.OrderBy(sortFields...)
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateCsWxMsgMatter updates CsWxMsgMatter by Id and returns error if
// the record to be updated doesn't exist
func UpdateCsWxMsgMatterById(m *CsWxMsgMatter) (err error) {
	o := orm.NewOrm()
	v := CsWxMsgMatter{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteCsWxMsgMatter deletes CsWxMsgMatter by Id and returns error if
// the record to be deleted doesn't exist
func DeleteCsWxMsgMatter(id int) (err error) {
	o := orm.NewOrm()
	v := CsWxMsgMatter{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&CsWxMsgMatter{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
