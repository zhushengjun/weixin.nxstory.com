package models

import (
	"fmt"

	"github.com/astaxie/beego/orm"
)

type CsAdminUser struct {
	Id       int    `orm:"column(id);auto" json:"id"`
	UserNmae string `orm:"column(username);size(50);null" json:"username"`
	PassWrod string `orm:"column(password);size(40);null" json:"password"`
}

func (t *CsAdminUser) TableName() string {
	return "cs_admin_user"
}

func init() {
	orm.RegisterModel(new(CsAdminUser))
}

// AddAdminUser insert a new CsAdminUser into database and returns
// last inserted Id on success.
func AddCsAdminUser(m *CsAdminUser) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// Get admin user with username and password
func GetAdminUserWithUserNameAndPassword(username, password string) (adminUser *CsAdminUser, err error) {
	o := orm.NewOrm()
	adminUser = &CsAdminUser{UserNmae: username, PassWrod: password}
	if err = o.Read(adminUser, "username", "password"); err == nil {
		return adminUser, nil
	} else {
		fmt.Println("Err get admin user->", err.Error())
		return nil, err
	}
}

// Get admin user with userid
func GetAdminUserWithUseId(userid int) (adminUser *CsAdminUser, err error) {
	o := orm.NewOrm()
	adminUser = &CsAdminUser{Id: userid}
	if err = o.Read(adminUser); err == nil {
		return adminUser, nil
	} else {
		fmt.Println("get user with id->", err.Error())
		return nil, err
	}

}
