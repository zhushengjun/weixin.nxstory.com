package models

import (
	"errors"
	"reflect"
	"strings"

	"github.com/astaxie/beego/orm"
)

type CsMaterial struct {
	Id      int    `orm:"column(id);auto"`
	WxAppId string `orm:"column(wxappid);size(50);null"`
	Mid     string `orm:"column(mid);size(40);null"`
	Mtype   string `orm:"column(mtype);size(20);null"`
	Content string `orm:"column(content);size(200);null"`
}

func (t *CsMaterial) TableName() string {
	return "cs_material"
}

func init() {
	orm.RegisterModel(new(CsMaterial))
}

// AddCsMaterial insert a new CsMaterial into database and returns
// last inserted Id on success.
func AddCsMaterial(m *CsMaterial) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetCsMaterialById retrieves CsMaterial by Id. Returns error if
// Id doesn't exist
func GetCsMaterialById(id int) (v *CsMaterial, err error) {
	o := orm.NewOrm()
	v = &CsMaterial{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllCsMaterial retrieves all CsMaterial matches certain condition. Returns empty list if
// no records exist
func GetAllCsMaterial(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(CsMaterial))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		qs = qs.Filter(k, v)
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}
	var l []CsMaterial
	qs = qs.OrderBy(sortFields...)
	if _, err := qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// GetCsMaterialById retrieves CsMaterial by Id. Returns error if
// Id doesn't exist
func GetCsMaterialByAppMId(appid, materialid string) (v *CsMaterial, err error) {
	o := orm.NewOrm()
	v = &CsMaterial{WxAppId: appid, Mid: materialid}
	if err = o.Read(v, "WxAppId", "Mid"); err == nil {
		return v, nil
	}
	return nil, err
}

// UpdateCsMaterial updates CsMaterial by Id and returns error if
// the record to be updated doesn't exist
func UpdateCsMaterialById(m *CsMaterial) (err error) {
	o := orm.NewOrm()
	v := CsMaterial{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		_, err = o.Update(m)

	}
	return
}
