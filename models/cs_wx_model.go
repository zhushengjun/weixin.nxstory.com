package models

import (
	"errors"
	"fmt"

	"github.com/astaxie/beego/orm"
)

// 微信模版表
type CsWxModel struct {
	Id int `orm:"column(id);auto"`
	//	WxAppId   string `orm:"column(model_weixin_id);size(255);null"`
	WxModelId string `orm:"column(model_id);size(255);null"`
	ModelType int    `orm:"column(model_type);size(11);null"`
	ModelName string `orm:"column(model_name);size(255);null"`
}

func (t *CsWxModel) TableName() string {
	return "cs_wx_model"
}

func init() {
	orm.RegisterModel(new(CsWxModel))
}

// get model_id with id
func GetModelIdWithId(id int) (modelId string) {
	wxModel, err := GetWXModel(id)
	if err != nil {
		fmt.Println("err->", err.Error())
		return ""
	} else {
		if wxModel == nil {
			return ""
		} else {
			return wxModel.WxModelId
		}
	}
}

// add model to db
func AddWXModel(model *CsWxModel) (id int64, err error) {
	o := orm.NewOrm()

	v := &CsWxModel{WxModelId: model.WxModelId}
	if err = o.Read(v, "model_id"); err == nil {
		return 0, errors.New("this appid has modelId:" + model.WxModelId)
	}
	id, err = o.Insert(model)

	return
}

// get model with id
func GetWXModel(id int) (v *CsWxModel, err error) {
	o := orm.NewOrm()
	v = &CsWxModel{Id: id}
	if err = o.Read(v, "id"); err == nil {
		return v, nil
	}
	return nil, err
}

// get model with modelId
func GetWXModelByModelId(appid, modelId string) (v []*CsWxModel, err error) {
	var models []*CsWxModel
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_model").Filter("model_weixin_id", appid).Filter("model_id", modelId).All(&models)
	if err != nil {
		return nil, err
	}
	return models, nil
}

// get all model with type
func GetWXModelWithType(appid, modelType string) (v []*CsWxModel, err error) {
	var models []*CsWxModel
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_model").Filter("model_weixin_id", appid).Filter("model_type", modelType).All(&models)
	if err != nil {
		return nil, err
	}
	return models, nil
}

// get all model with name
func GetWXModelWithModelName(appid, modelName string) (v []*CsWxModel, err error) {
	var models []*CsWxModel
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_model").Filter("model_weixin_id", appid).Filter("model_name", modelName).All(&models)
	if err != nil {
		return nil, err
	}
	return models, nil
}

// get all model with modelId
func GetWXModelWithModelId(appid, modelId string) (v []*CsWxModel, err error) {
	var models []*CsWxModel
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_model").Filter("model_weixin_id", appid).Filter("model_id", modelId).All(&models)
	if err != nil {
		return nil, err
	}
	return models, nil
}

// get all model with modelid modelName
func GetWXModelWithIdAndName(appid, modelId, modelName string) (v []*CsWxModel, err error) {
	var models []*CsWxModel
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_model").Filter("model_weixin_id", appid).Filter("model_id", modelId).Filter("model_name", modelName).All(&models)
	if err != nil {
		return nil, err
	}
	return models, nil
}

// get all model with modelid modelType
func GetWXModelWithIdAndType(appid, modelId, modelType string) (v []*CsWxModel, err error) {
	var models []*CsWxModel
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_model").Filter("model_weixin_id", appid).Filter("model_id", modelId).Filter("model_type", modelType).All(&models)
	if err != nil {
		return nil, err
	}
	return models, nil
}

// get all model with modelName modelType
func GetWXModelWithNameAndType(appid, modelName, modelType string) (v []*CsWxModel, err error) {
	var models []*CsWxModel
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_model").Filter("model_weixin_id", appid).Filter("model_name", modelName).Filter("model_type", modelType).All(&models)
	if err != nil {
		return nil, err
	}
	return models, nil
}

// get all model with modelid name type
func GetWXModelWithIdAndNameAndType(appid, modelId, modelName, modelType string) (v []*CsWxModel, err error) {
	var models []*CsWxModel
	o := orm.NewOrm()
	if modelId != "" && modelName != "" && modelType != "" {
		_, err = o.QueryTable("cs_wx_model").Filter("model_weixin_id", appid).Filter("model_id", modelId).Filter("model_name", modelName).Filter("model_type", modelType).All(&models)
		if err != nil {
			return nil, err
		}
	} else if modelId != "" && modelName != "" && modelType == "" {
		return GetWXModelWithIdAndName(appid, modelId, modelName)
	} else if modelId != "" && modelName == "" && modelType != "" {
		return GetWXModelWithIdAndType(appid, modelId, modelType)
	} else if modelId == "" && modelName != "" && modelType != "" {
		return GetWXModelWithNameAndType(appid, modelName, modelType)
	} else if modelId != "" && modelName == "" && modelType == "" {
		return GetWXModelByModelId(appid, modelId)
	} else if modelId == "" && modelName != "" && modelType == "" {
		return GetWXModelWithModelName(appid, modelName)
	} else if modelId == "" && modelName == "" && modelType != "" {
		return GetWXModelWithType(appid, modelType)
	}
	return models, nil
}

// update model with id
func UpdateWXModelById(m *CsWxModel) (err error) {
	o := orm.NewOrm()
	v := CsWxModel{Id: m.Id}
	if err = o.Read(&v); err == nil {
		_, err = o.Update(m)
	}
	return
}

// delete model with id
func DeleteModelById(m *CsWxModel) (err error) {
	o := orm.NewOrm()
	v := CsWxModel{Id: m.Id}
	if err = o.Read(&v); err == nil {
		_, err = o.Delete(m)
	}
	return
}

// delete model with modelId and appid
func DeleteModelByModelId(m *CsWxModel) (err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_model").Filter("model_id", m.WxModelId).Delete()
	return
}

// get all model
func GetAllModel() (items []*CsWxModel, err error) {
	var model []*CsWxModel
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_wx_model").All(&model)
	if err != nil {
		return nil, err
	} else {
		return model, nil
	}
}
func GetModelNameByModelId(modelId string) (modelName string, err error) {
	o := orm.NewOrm()
	v := &CsWxModel{WxModelId: modelId}
	if err = o.Read(v, "model_id"); err == nil {
		return v.ModelName, nil
	} else {
		return "", err
	}
}

func GetModelTypeByModelId(modelId string) (modelType int, err error) {
	o := orm.NewOrm()
	v := &CsWxModel{WxModelId: modelId}
	if err = o.Read(v, "model_id"); err == nil {
		return v.ModelType, nil
	} else {
		return 0, err
	}
}
