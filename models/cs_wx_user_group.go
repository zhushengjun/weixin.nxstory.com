package models

import (
	"time"

	"github.com/astaxie/beego/orm"
)

type CsUserGroup struct {
	Id         int       `orm:"column(id);auto"`
	WxAppId    string    `orm:"column(wxappid);size(50);null"`
	OpenId     string    `orm:"column(openid);size(40);null"`
	CreateTime time.Time `orm:"column(create_time);type(datetime);null"`
	Group      int       `orm:"column(group);null"`
	UpdateTime time.Time `orm:"column(update_time);type(datetime);null"`
}

func (t *CsUserGroup) TableName() string {
	return "cs_user_group"
}

func init() {
	orm.RegisterModel(new(CsUserGroup))
}

// AddCsUser insert a new CsUser into database and returns
// last inserted Id on success.
func AddCsUserGroup(m *CsUserGroup) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetCsUserGroupById retrieves CsUserGroup by Id. Returns error if
// Id doesn't exist
func GetCsUserGroupById(id int) (v *CsUserGroup, err error) {
	o := orm.NewOrm()
	v = &CsUserGroup{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetCsUserGroupById retrieves CsUserGroup by Id. Returns error if
// Id doesn't exist
func GetCsUserGroupByAppOpenId(appid, openid string) (v *CsUserGroup, err error) {
	o := orm.NewOrm()
	v = &CsUserGroup{WxAppId: appid, OpenId: openid}
	if err = o.Read(v, "WxAppId", "OpenId"); err == nil {
		return v, nil
	}
	return nil, err
}

// UpdateCsUserGroup updates CsUserGroup by Id and returns error if
// the record to be updated doesn't exist
func UpdateCsUserGroupById(m *CsUserGroup) (err error) {
	o := orm.NewOrm()
	v := CsUserGroup{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		_, err = o.Update(m)

	}
	return
}
