package models

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"github.com/astaxie/beego/orm"
)

type CsKeywordsOperate struct {
	Id         int    `orm:"column(id);auto"`
	Keyword    string `orm:"column(keyword);size(255);null"`
	Resultword string `orm:"column(resultword);null"`
	WxAppId    string `orm:"column(appid);size(50);null"`
}

func (t *CsKeywordsOperate) TableName() string {
	return "cs_keywords_operate"
}

func init() {
	orm.RegisterModel(new(CsKeywordsOperate))
}

// AddCsKeywordsOperate insert a new CsKeywordsOperate into database and returns
// last inserted Id on success.
func AddCsKeywordsOperate(m *CsKeywordsOperate) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// get keyword info with id and appid
func GetKeywordWithIdAndAppid(id int, appid string) (v *CsKeywordsOperate, err error) {
	o := orm.NewOrm()
	Operate := CsKeywordsOperate{}
	err = o.QueryTable("cs_keywords_operate").Filter("id", id).Filter("appid", appid).One(&Operate)
	if err != nil {
		return nil, err
	} else {
		return &Operate, nil
	}
}

// get all keyword with appid
func GetAllKeyReplyWithAppid(keyword, appid string) (keywordList []CsKeywordsOperate, err error) {
	o := orm.NewOrm()
	if keyword != "" && appid != "" {
		_, err = o.QueryTable("cs_keywords_operate").Filter("keyword", keyword).Filter("appid", appid).OrderBy("appid").All(&keywordList)
	} else if appid != "" {
		_, err = o.QueryTable("cs_keywords_operate").Filter("appid", appid).OrderBy("appid").All(&keywordList)
	} else if keyword != "" {
		_, err = o.QueryTable("cs_keywords_operate").Filter("keyword", keyword).OrderBy("appid").All(&keywordList)
	} else {
		_, err = o.QueryTable("cs_keywords_operate").OrderBy("appid").All(&keywordList)
	}
	if err != nil {
		fmt.Println("GetAllKeyReplyWithAppid err ->", err.Error())
		return nil, err
	} else {
		return keywordList, nil
	}

}

// get cs keyword info with appid and content
func GetCsKeyWordWithAppidAndContent(appid, content string) (v *CsKeywordsOperate, err error) {
	o := orm.NewOrm()
	Operate := CsKeywordsOperate{}
	err = o.QueryTable("cs_keywords_operate").Filter("keyword", content).Filter("appid", appid).One(&Operate)
	if err != nil {
		return nil, err
	} else {
		return &Operate, nil
	}
}

// GetCsKeywordsOperateById retrieves CsKeywordsOperate by Id. Returns error if
// Id doesn't exist
func GetCsKeywordsOperateById(id int) (v *CsKeywordsOperate, err error) {
	o := orm.NewOrm()
	v = &CsKeywordsOperate{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllCsKeywordsOperate retrieves all CsKeywordsOperate matches certain condition. Returns empty list if
// no records exist
func GetAllCsKeywordsOperate(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(CsKeywordsOperate))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		if strings.Contains(k, "isnull") {
			qs = qs.Filter(k, (v == "true" || v == "1"))
		} else {
			qs = qs.Filter(k, v)
		}
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []CsKeywordsOperate
	qs = qs.OrderBy(sortFields...)
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateCsKeywordsOperate updates CsKeywordsOperate by Id and returns error if
// the record to be updated doesn't exist
func UpdateCsKeywordsOperateById(m *CsKeywordsOperate) (err error) {
	o := orm.NewOrm()
	v := CsKeywordsOperate{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteCsKeywordsOperate deletes CsKeywordsOperate by Id and returns error if
// the record to be deleted doesn't exist
func DeleteCsKeywordsOperate(id int) (err error) {
	o := orm.NewOrm()
	v := CsKeywordsOperate{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&CsKeywordsOperate{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
