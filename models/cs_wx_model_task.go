package models

import (
	"fmt"
	"time"

	"github.com/astaxie/beego/orm"
)

// 发送模版任务表
type CsModelTask struct {
	Id        int       `orm:"column(id);auto"`
	SendType  int       `orm:"column(send_type);size(11);null"`                 //任务发送类型（0：只发一次1：每天发送 2：每周1发送...后期可以添加）
	SendTime  time.Time `orm:"column(send_time);size(255);type(datetime);null"` //发送时间 和Type对应（2017-07-21 12:00:00）
	SendTag   int       `orm:"column(send_tag);size(11);null"`                  //发送的人标签(1:男 2:女...后期可以扩展)
	ModelId   string    `orm:"column(model_id);size(255);null"`                 //微信模版ID
	SendJson  string    `orm:"column(send_json);size(255);null"`                //发送请求的json体
	SendAppId string    `orm:"column(send_wx_id);size(255);null"`               // 要发送的公众号的信息集合
}

func (t *CsModelTask) TableName() string {
	return "cs_model_task"
}

func init() {
	orm.RegisterModel(new(CsModelTask))
}

// add Task to db
func AddModelTask(model *CsModelTask) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(model)
	fmt.Println("**************", model.SendTime)
	return
}

// get Task with id
func GetModelTaskById(id int) (v *CsModelTask, err error) {
	o := orm.NewOrm()
	v = &CsModelTask{Id: id}
	if err = o.Read(v, "id"); err == nil {
		return v, nil
	}
	return nil, err
}

// get all Task
func GetAllModelTask() (Tasks []CsModelTask, err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable("cs_model_task").All(&Tasks)
	return
}

// delete Task with id
func DeleteModelTaskById(id int) (err error) {
	o := orm.NewOrm()
	v := &CsModelTask{Id: id}
	if err = o.Read(v, "id"); err == nil {
		_, err = o.Delete(v)
	}
	return
}

// update Task with id
func UpdateModelTaskById(model *CsModelTask) (err error) {
	o := orm.NewOrm()
	v := CsModelTask{Id: model.Id}
	if err = o.Read(&v); err == nil {
		_, err = o.Update(model)
	}
	return
}
