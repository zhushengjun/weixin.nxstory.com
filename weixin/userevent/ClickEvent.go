package userevent

import (
	"fmt"
	"log"
	"time"

	"github.com/chanxuehong/wechat.v2/mp/core"
	"github.com/chanxuehong/wechat.v2/mp/menu"
	"github.com/chanxuehong/wechat.v2/mp/message/callback/response"
	"weixin.nxstory.com/models"
)

// 用户点击菜单拉取消息事件推送
func UserClickMenuEvent(ctx *core.Context) {
	log.Printf("用户点击按钮事件:\n%s\n", ctx.MsgPlaintext)
	var resContent string
	event := menu.GetClickEvent(ctx.MixedMsg)
	Key := event.EventKey
	fmt.Println("Key->", Key)
	wxinfo, _ := models.GetCsWxAccountByOriginalId(event.ToUserName)
	fmt.Println("wxinfo->", wxinfo)
	if wxinfo != nil {
		menuInfo, err := models.GetMenuInfoWithKey(wxinfo.WxAppId, Key)
		if menuInfo != nil {
			fmt.Println("menuinfo->", menuInfo)
			resContent = menuInfo.ResContent
			resp := response.NewText(event.FromUserName, event.ToUserName, event.CreateTime, resContent)
			ctx.RawResponse(resp) // 明文回复
		} else {
			fmt.Println("err->", err.Error())
		}
		ReqUser, _ := models.GetWXUserWithOpenId(event.FromUserName)
		if ReqUser != nil {
			ReqUser.LastMutualTime = time.Now()
			models.UpdateWXUserWithId(ReqUser)
		}
	} else {
		fmt.Println("wxinfo 为空")
	}
}
