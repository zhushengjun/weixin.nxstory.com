package userevent

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/chanxuehong/wechat.v2/mp/core"
	"github.com/chanxuehong/wechat.v2/mp/menu"
	"github.com/chanxuehong/wechat.v2/mp/message/callback/response"
	"weixin.nxstory.com/models"
	Comm "weixin.nxstory.com/weixin/common"
)

// 用户详情返回Bean
type UserRes struct {
	SubScribe  int    `json:"subscribe"`
	Sex        int    `json:"sex"`
	OpenId     string `json:"openid"`
	Nickname   string `json:"nickname"`
	Language   string `json:"language"`
	City       string `json:"city"`
	Province   string `json:"province"`
	Country    string `json:"country"`
	Headimgurl string `json:"headimgurl"`
	Remark     string `json:"remark"`
}

// 用户详情返回错误
type ResultErr struct {
	Code int    `json:"errcode"` // 错误码
	Msg  string `json:"errmsg"`  // 错误信息
}

//用户取消关注时的回复
func UnSubscribeEventHandler(ctx *core.Context) {
	log.Printf("用户取消关注:\n%s\n", ctx.MsgPlaintext)
	event := menu.GetClickEvent(ctx.MixedMsg)
	openId := event.FromUserName
	originalId := event.ToUserName
	wxinfo, _ := models.GetCsWxAccountByOriginalId(originalId)
	if wxinfo != nil {
		ReqUser, _ := models.GetWXUserWithOpenId(openId)
		if ReqUser != nil {
			ReqUser.UnSubScribeTime = time.Now()
			ReqUser.SubScribeStatus = 1
			ReqUser.LastMutualTime = time.Now()
			models.UpdateWXUserWithId(ReqUser)
		}
		resp := response.NewText(event.FromUserName, event.ToUserName, event.CreateTime, "欢迎再次关注公众号")
		ctx.RawResponse(resp) // 明文回复
	}
}

//用户关注时的回复
func SubscribeEventHandler(ctx *core.Context) {
	log.Printf("用户关注事件:\n%s\n", ctx.MsgPlaintext)
	event := menu.GetClickEvent(ctx.MixedMsg)
	openId := event.FromUserName
	OriginalId := event.ToUserName
ContimueGetToken:
	ReqUser, _ := models.GetWXUserWithOpenId(openId)
	wxInfo, _ := models.GetCsWxAccountByOriginalId(OriginalId)
	if ReqUser == nil { // 第一次关注
		if wxInfo != nil {
			fmt.Println("AppSecret->", wxInfo.AppSecret)
			AccessToken, _ := Comm.GetTokenFromRedis(wxInfo.WxAppId, wxInfo.AppSecret)
			fmt.Println("AccessToken->", AccessToken)
			User, code := GetUserInfoByOpenId(AccessToken, openId)
			if code == 0 { // 0 获取成功 1 token错误 -1 openid 错误 -2 两次解析错误（返回） 不管
				userDB := models.CsWxUsers{}
				userDB.AppId = wxInfo.WxAppId
				userDB.Openid = openId
				userDB.Nickname = User.Nickname
				userDB.LastMutualTime = time.Now()
				userDB.Sex = User.Sex
				userDB.Language = User.Language
				userDB.City = User.City
				userDB.Province = User.Province
				userDB.Country = User.Country
				userDB.HeadImg = User.Headimgurl
				userDB.CreateTime = time.Now()
				userDB.SubScribeTime = time.Now()
				userDB.SubScribeStatus = 0
				uid, err := models.AddWxUsers(&userDB)
				fmt.Println("uid->", uid)
				if err != nil {
					fmt.Println("err->", err.Error())
				}
			} else if code == 1 {
				models.DeleteCache(wxInfo.WxAppId)
			}
			goto ContimueGetToken
		}
	} else { // 非第一次关注
		ReqUser.SubScribeTime = time.Now()
		ReqUser.SubScribeStatus = 0
		ReqUser.LastMutualTime = time.Now()
		models.UpdateWXUserWithId(ReqUser)
	}
	ManagerUserTagWithSex(wxInfo.WxAppId, wxInfo.AppSecret, openId, ReqUser.Sex)
	resp := response.NewText(event.FromUserName, event.ToUserName, event.CreateTime, "欢迎关注公众号1111")
	ctx.RawResponse(resp) // 明文回复
}

// 通过用户opneid获取详情
func GetUserInfoByOpenId(accessToken, openId string) (u *UserRes, code int) {
	if openId != "" {
		fmt.Println("openid->", openId)
		fmt.Println("UrlGetUser->", Comm.UrlGetUser)
		UrlGetUserInfo := fmt.Sprintf(Comm.UrlGetUser, accessToken, openId)
		user := UserRes{}
		res3, err := http.Get(UrlGetUserInfo)
		fmt.Println("UrlGetUserInfo->", UrlGetUserInfo)
		resStr3, err := ioutil.ReadAll(res3.Body)
		res3.Body.Close()
		if err == nil {
			err = json.Unmarshal(resStr3, &user)
			if err != nil { // 返回错误 通过错误返回实体类解析
				fmt.Println("err3->", err.Error())
				resErr := ResultErr{}
				err := json.Unmarshal(resStr3, &resErr)
				if err == nil {
					if resErr.Code == 40001 || resErr.Code == 40014 || resErr.Code == 40002 { //Token 不对
						return nil, 1
					}
				}
				return nil, -2
			} else {
				fmt.Println("resStr->", string(resStr3))
				return &user, 0
			}
		}
	}
	return nil, -1
}

// 通过sex 为粉丝打标签
func ManagerUserTagWithSex(appid, secret, openId string, userSex int) {
	var tagId int
	Url := Comm.ManagerTagsForUser
	param := make(map[string]interface{}, 0)
	openList := make([]string, 0)
	TagList, _ := models.GetAllGroupsWithNoParam(appid)
	if userSex == 1 {
		for _, value := range TagList {
			if value.Name == "男性用户" {
				tagId = value.Group
			}
		}
	} else if userSex == 2 {
		for _, value := range TagList {
			if value.Name == "女性用户" {
				tagId = value.Group
			}
		}
	} else {
		for _, value := range TagList {
			if value.Name == "未知用户" {
				tagId = value.Group
			}
		}
	}
	token, _ := Comm.GetTokenFromRedis(appid, secret)
	reqUrl := Url + token
	openList = append(openList, openId)
	param["tagid"] = tagId
	param["openid_list"] = openList
	paramJson, _ := json.Marshal(param)
	fmt.Println("url->", reqUrl)
	fmt.Println("param->", string(paramJson))
	request, err := http.Post(reqUrl, "application/x-www-form-urlencoded", strings.NewReader(string(paramJson)))
	if err != nil {
		fmt.Println(err)
	}

	defer request.Body.Close()
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		// handle error
	}
	fmt.Println(string(body))
}
