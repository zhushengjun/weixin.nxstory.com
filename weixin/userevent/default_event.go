package userevent

import (
	"log"

	"github.com/chanxuehong/wechat.v2/mp/core"
)

//没有匹配消息
func DefaultEventHandler(ctx *core.Context) {
	log.Printf("收到事件:\n%s\n", ctx.MsgPlaintext)
	ctx.NoneResponse()
}
