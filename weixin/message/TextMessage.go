package message

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/chanxuehong/wechat.v2/mp/core"
	"github.com/chanxuehong/wechat.v2/mp/message/callback/request"
	"github.com/chanxuehong/wechat.v2/mp/message/callback/response"
	"weixin.nxstory.com/models"
)

//收到文本消息回复
func TextMsgHandler(ctx *core.Context) {
	log.Printf("收到文本消息:\n%s\n", ctx.MsgPlaintext)
	msg := request.GetText(ctx.MixedMsg)
	content := msg.Content
	originalId := msg.ToUserName
	wxinfo, _ := models.GetCsWxAccountByOriginalId(originalId)
	if wxinfo != nil {
		textMsg := response.Text{}
		content = strings.Replace(content, " ", "", -1)
		content = strings.Replace(content, "　", "", -1)
		content = strings.Replace(content, ",", "", -1)
		content = strings.Replace(content, "，", "", -1)
		content = strings.Replace(content, ".", "", -1)
		content = strings.Replace(content, "。", "", -1)
		keywordInfo, err := models.GetCsKeyWordWithAppidAndContent(wxinfo.WxAppId, content)
		if keywordInfo != nil {
			textMsg.Content = keywordInfo.Resultword
		} else {
			fmt.Println("err->", err.Error())
			textMsg.Content = "您有新的消息1　<a href='www.baidu.com'>点击查看</a>"
		}
		resp := response.NewText(msg.FromUserName, msg.ToUserName, msg.CreateTime, textMsg.Content)
		if resp != nil {
			fmt.Println("明文回复->", resp)
		}
		ctx.RawResponse(resp) // 明文回复
		ReqUser, _ := models.GetWXUserWithOpenId(msg.FromUserName)
		if ReqUser != nil {
			ReqUser.LastMutualTime = time.Now()
			models.UpdateWXUserWithId(ReqUser)
		}
	}
}
