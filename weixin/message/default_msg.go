package message

import (
	"log"

	"github.com/chanxuehong/wechat.v2/mp/core"
)

//其他类型的消息
func DefaultMsgHandler(ctx *core.Context) {
	log.Printf("收到消息:\n%s\n", ctx.MsgPlaintext)
	ctx.NoneResponse()
}
