package main

import (
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/chanxuehong/wechat.v2/mp/core"
	"github.com/chanxuehong/wechat.v2/mp/menu"
	"github.com/chanxuehong/wechat.v2/mp/message/callback/request"
	_ "github.com/go-sql-driver/mysql"
	"weixin.nxstory.com/models"
	Msg "weixin.nxstory.com/weixin/message"
	Event "weixin.nxstory.com/weixin/userevent"
)

const (
	accessTokenFetchUrl = "https://api.weixin.qq.com/cgi-bin/token"
	wxOriId             = "gh_cec99945fc9c"
	wxToken             = "ktfFKGL0ypf3chTo60GsMhTOQyrh5SLY"
	wxEncodedAESKey     = ""
)

var (
	msgHandler core.Handler

	msgServer *core.Server
)

// 注册事件
func init() {
	http.HandleFunc("/wx_callback", wxCallbackHandler)
	mux := core.NewServeMux()
	mux.DefaultMsgHandleFunc(Msg.DefaultMsgHandler)
	mux.DefaultEventHandleFunc(Event.DefaultEventHandler)
	mux.EventHandleFunc(request.EventTypeSubscribe, Event.SubscribeEventHandler)     // 关注
	mux.EventHandleFunc(request.EventTypeUnsubscribe, Event.UnSubscribeEventHandler) // 取关
	mux.EventHandleFunc(menu.EventTypeClick, Event.UserClickMenuEvent)               // 点击菜单拉取
	mux.MsgHandleFunc(request.MsgTypeText, Msg.TextMsgHandler)                       // 文字消息
	msgHandler = mux
}

// 注册数据库
func init() {
	orm.RegisterDataBase("default", "mysql", fmt.Sprintf(beego.AppConfig.String("db::slave_operation"), url.QueryEscape("Asia/Shanghai")), 3, 15)
	orm.RegisterDataBase("m_operation", "mysql", fmt.Sprintf(beego.AppConfig.String("db::master_operation"), url.QueryEscape("Asia/Shanghai")), 3, 15)
	orm.DefaultTimeLoc, _ = time.LoadLocation("Asia/Shanghai")
}

func main() {
	fmt.Println("start")
	http.ListenAndServe(":8080", nil)
}

// 注册回调
func wxCallbackHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	appid := r.Form["appid"][0]
	fmt.Println("appid->", appid)
	wxInfo, _ := models.GetCsWxAccountByAppId(appid)
	if wxInfo != nil {
		msgServer = core.NewServer(wxInfo.OriginalId, "", wxToken, wxEncodedAESKey, msgHandler, nil)
		msgServer.ServeHTTP(w, r, nil)
	}
}

//// 验证Token
//func fetchAccessToken() (string, float64, error) {
//	requestLine := strings.Join([]string{accessTokenFetchUrl,
//		"?grant_type=client_credential&appid=",ls
//		wxAppId,
//		"&secret=",
//		wxAppSecret}, "")
//	resp, err := http.Get(requestLine)
//	if err != nil || resp.StatusCode != http.StatusOK {
//		return "", 0.0, err
//	}

//	defer resp.Body.Close()
//	body, err := ioutil.ReadAll(resp.Body)
//	if err != nil {
//		return "", 0.0, err
//	}

//	fmt.Println(string(body))
//	return string(body), 0.0, err
//}
