package common

const (
	// 请求token地址
	GetWXTokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s"
	// 请求用户详情的地址
	UrlGetUser = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s&lang=zh_CN"
	// 请求管理分组标签地址
	ManagerTagsForUser = "https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token="
)
