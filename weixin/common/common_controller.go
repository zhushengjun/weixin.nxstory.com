package common

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"encoding/json"
	"time"

	"github.com/astaxie/beego"
	"weixin.nxstory.com/models"
)

type CommonController struct {
	beego.Controller
}

// 请求Token返回
type AccessTokenResponse struct {
	Access_token string `json:"access_token"`
	Expires_in   int    `json:"expires_in"`
}
type TokenStr struct {
	WXToken     string `json:"wx_token"`
	WXTokenTime int64  `json:"wx_time"`
}

// get token from redis
func GetTokenFromRedis(appid, appsecret string) (token string, err error) {
	apptoken := models.GetCache(appid)
	if apptoken != nil {
		return string(apptoken), err
	} else {
		token, err := GetTokenFromHttp(appid, appsecret)
		if err != nil {
			return "", err
		} else {
			models.PutCache(appid, []byte(token), 7000)
			return token, nil
		}
	}
}

// 获取Token（通过请求）
func GetTokenFromHttp(appid, appSecret string) (appToken string, err error) {
	Url := fmt.Sprintf(GetWXTokenUrl, appid, appSecret)
	Access := AccessTokenResponse{}
	fmt.Println("Url->", Url)
	res, err := http.Get(Url)
	resStr, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err == nil {
		err = json.Unmarshal(resStr, &Access)
		if err != nil {
			fmt.Println("err1->", err.Error())
			return "", err
		}
	} else {
		fmt.Println("err2->", err.Error())
		return "", err
	}
	SetSessToekn := TokenStr{}
	SetSessToekn.WXToken = Access.Access_token
	SetSessToekn.WXTokenTime = time.Now().Unix()
	return Access.Access_token, nil
}
