package routers

import (
	"github.com/astaxie/beego"
	"weixin.nxstory.com/manager/controllers"
)

func init() {
	beego.Router("/", &controllers.IndexController{})                          //首页
	beego.Router("/user/login", &controllers.UserController{}, "get:Login")    // 登陆页
	beego.Router("/user/login", &controllers.UserController{}, "post:Logindo") // 登录提交操作
	beego.Router("/user/logout", &controllers.UserController{}, "*:Logout")    // 登出

	beego.Router("/manager/replymanager", &controllers.ReplyManagerController{}, "get:GetList")    // 关键字回复首页
	beego.Router("/replymanager/add", &controllers.ReplyManagerController{}, "get:AddReply")       // 关键字回复添加
	beego.Router("/replymanager/add", &controllers.ReplyManagerController{}, "post:AddReplyDo")    // 关键字回复添加提交
	beego.Router("replymanager/edit", &controllers.ReplyManagerController{}, "get:EditReply")      // 关键字回复编辑
	beego.Router("replymanager/edit", &controllers.ReplyManagerController{}, "post:EditReplyDo")   // 关键字回复编辑
	beego.Router("/replymanager/delete", &controllers.ReplyManagerController{}, "*:DeleteReplyDo") // 关键字回复删除

	beego.Router("/manager/menu", &controllers.MenuManagerController{})                                 // 菜单管理
	beego.Router("/manager/menu/list", &controllers.MenuManagerController{}, "get:ListMenu")            // 菜单管理 查看
	beego.Router("/manager/menu/edit", &controllers.MenuManagerController{}, "get:EditMenu")            // 菜单管理 编辑
	beego.Router("/manager/menu/edit", &controllers.MenuManagerController{}, "post:EditMenuDo")         // 菜单管理 编辑提交
	beego.Router("/manager/menu/deletemenu", &controllers.MenuManagerController{}, "*:DeleteMenu")      // 菜单管理 编辑提交
	beego.Router("/manager/menu/submenu", &controllers.MenuManagerController{}, "get:ListSubMenu")      // 菜单管理 查看二级菜单
	beego.Router("/manager/menu/addSubmenu", &controllers.MenuManagerController{}, "get:AddSubMenu")    // 菜单管理 添加二级菜单
	beego.Router("/manager/menu/addSubmenu", &controllers.MenuManagerController{}, "post:AddSubMenuDo") // 菜单管理 添加二级菜单提交

	beego.Router("/manager/menu/update", &controllers.MenuManagerController{}, "*:UpdateMenuDo") // 菜单管理 将菜单内容更新到微信后台

	beego.Router("/manager/message", &controllers.MessageController{})                                     // 文字和图文消息管理  消息管理
	beego.Router("/manager/message/delete", &controllers.MessageController{}, "*:DeleteMessage")           // 文字和图文消息管理  删除消息
	beego.Router("/matter/word/add", &controllers.MessageController{}, "get:AddTestMessage")               // 文字和图文消息管理  添加文字消息
	beego.Router("/matter/word/add", &controllers.MessageController{}, "post:AddTestMessageDo")            // 文字和图文消息管理  添加文字消息提交
	beego.Router("/manager/message/text/edit", &controllers.MessageController{}, "get:EditTextMessage")    // 文字和图文消息管理  修改文字消息
	beego.Router("/manager/message/text/edit", &controllers.MessageController{}, "post:EditTextMessageDo") // 文字和图文消息管理  修改文字消息提交
	beego.Router("/matter/img/add", &controllers.MessageController{}, "get:AddImgMessage")                 // 文字和图文消息管理  添加图文消息
	beego.Router("/matter/img/add", &controllers.MessageController{}, "post:AddImgMesssageDo")             // 文字和图文消息管理  添加文字消息提交
	beego.Router("/manager/message/img/edit", &controllers.MessageController{}, "get:EditImgMessage")      // 文字和图文消息管理  修改图文消息
	beego.Router("/manager/message/img/edit", &controllers.MessageController{}, "post:EditImgMessageDo")   // 文字和图文消息管理  修改文字消息提交

	beego.Router("/manager/sengmsg", &controllers.SendMsgController{})                      // 发送任务首页
	beego.Router("/manager/sengmsg/add", &controllers.SendMsgController{}, "get:Add")       // 添加发送任务
	beego.Router("/manager/sengmsg/add", &controllers.SendMsgController{}, "post:AddDo")    // 添加发送任务提交
	beego.Router("/manager/sengmsg/delete", &controllers.SendMsgController{}, "get:Delete") // 删除发送任务

	beego.Router("/material/text", &controllers.TextMaterialController{})
	beego.Router("/material/text/do", &controllers.TextMaterialController{}, "post:AddMaterialDo")
	beego.Router("/material/text/list", &controllers.TextMaterialController{}, "*:WXTextMaterList")
	beego.Router("/material/text/sendtest", &controllers.TextMaterialController{}, "*:SendTextTest")
	beego.Router("/material/text/sendtest/do", &controllers.TextMaterialController{}, "post:SendTextTestDo")
	beego.Router("/material/text/sendgroup", &controllers.TextMaterialController{}, "*:SendTextGroup")
	beego.Router("/material/text/sendgroup/do", &controllers.TextMaterialController{}, "post:SendTextGroupDo")
	beego.Router("/material/text/edit", &controllers.TextMaterialController{}, "get:EditTextMatreial")
	beego.Router("/material/text/edit/do", &controllers.TextMaterialController{}, "post:EditTextMatreialDo")

	beego.Router("/weixin", &controllers.WeixinController{})
	beego.Router("/weixin/material/:appid:string", &controllers.WeixinController{}, "*:Material")
	beego.Router("/weixin/materialdown/:appid:string", &controllers.WeixinController{}, "*:MaterialDown")
	beego.Router("/weixin/groupdown/:appid:string", &controllers.WeixinController{}, "*:GroupDown")
	beego.Router("/weixin/sendgroup/:id:int", &controllers.WeixinController{}, "get:SendGroup")
	beego.Router("/weixin/sendall/:id:int", &controllers.WeixinController{}, "get:SendAll")
	beego.Router("/weixin/sendgroup/:id:int", &controllers.WeixinController{}, "post:SendGroupDo")
	beego.Router("/weixin/sendall/:id:int", &controllers.WeixinController{}, "post:SendAllDo")
	beego.Router("/weixin/add/onlyid", &controllers.WeixinController{}, "get:AddOnlyId")
	beego.Router("/weixin/add/onlyid", &controllers.WeixinController{}, "post:AddOnlyIdDo")

}
