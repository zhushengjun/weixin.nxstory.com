package controllers

import (
	"fmt"
)

type IndexController struct {
	CommonController
}

func (c *IndexController) Get() {
	c.CheckLogin()
	fmt.Println("index")
	c.Data["Title"] = "控制面板"
	c.Data["TitleSmall"] = "信息"
	c.Data["Title"] = "首页"
	c.TplName = "index.html"
	c.Layout = "layout.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["HeaderExt"] = "headerext_main.html"
	c.LayoutSections["FooterExt"] = "footerext_main.html"
}
