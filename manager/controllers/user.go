package controllers

import (
	"fmt"

	"github.com/astaxie/beego"
	"weixin.nxstory.com/models"
)

type UserController struct {
	CommonController
}

func (this *UserController) Login() {
	this.Data["Title"] = "登录"
	this.TplName = "user/login.html"
	this.Layout = "layout_login.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_login.html"
	this.LayoutSections["FooterExt"] = "footerext_login.html"
}
func (this *UserController) Logindo() {
	username := this.GetString("username")
	password := this.GetString("password")
	userinfo, _ := models.GetAdminUserWithUserNameAndPassword(username, password)
	fmt.Println("userinfo->", userinfo)
	if userinfo != nil {
		this.SetSession(beego.AppConfig.String("admin_user_id"), userinfo.Id)
		this.Redirect("/", 302)
	} else {
		this.Redirect("/user/login", 302)
	}
}
func (this *UserController) Logout() {
	this.SetSession("adminuser", "")
	this.SetSession("admintoken", "")
	this.Redirect("/user/login", 302)
}
