package controllers

import (
	"fmt"
	"strings"
	"time"

	"github.com/chanxuehong/wechat.v2/mp/core"
	"github.com/chanxuehong/wechat.v2/mp/message/mass/mass2group"
	"github.com/chanxuehong/wechat.v2/mp/message/mass/preview"
	"weixin.nxstory.com/models"
)

type TextMaterialController struct {
	CommonController
}

// 添加文字素材
func (this *TextMaterialController) Get() {
	this.CheckLogin()
	accountList, _ := models.GetAllCsWeixinAccount()
	this.Data["items"] = accountList
	this.Data["Title"] = "添加素材"
	this.Data["TitleSmall"] = "文字素材"
	this.TplName = "material/add_text_material.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_list.html"
	this.LayoutSections["FooterExt"] = "footerext_list.html"
}

// 添加文字素材提交
func (this *TextMaterialController) AddMaterialDo() {
	this.CheckLogin()
	WxAppID := this.GetString("appid")
	Content := this.GetString("content")
	Remark := this.GetString("remark")
	TextMaterInfo := models.CsTextMater{}
	TextMaterInfo.Mtype = "text"
	TextMaterInfo.Wxappid = WxAppID
	TextMaterInfo.Content = Content
	TextMaterInfo.Remark = Remark
	TextMaterInfo.CreateTime = time.Now()
	_, err := models.AddCsTextMater(&TextMaterInfo)
	if err != nil {
		fmt.Println("err->", err.Error())
	}
	this.Redirect("/material/text/list?wxappid="+WxAppID, 302)
}

// 编辑文字素材
func (this *TextMaterialController) EditTextMatreial() {
	this.CheckLogin()
	materialid, _ := this.GetInt("materialid")
	MaterialInfo, _ := models.GetCsTextMaterById(materialid)
	wxinfo, _ := models.GetCsWxAccountByAppId(MaterialInfo.Wxappid)
	fmt.Println("matercontent->", MaterialInfo.Content)
	this.Data["wxinfo"] = wxinfo
	this.Data["item"] = MaterialInfo
	this.Data["Title"] = "素材详情"
	this.Data["TitleSmall"] = "编辑素材"
	this.TplName = "material/edit_text_material.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_list.html"
	this.LayoutSections["FooterExt"] = "footerext_list.html"
}

// 编辑文字素材提交
func (this *TextMaterialController) EditTextMatreialDo() {
	this.CheckLogin()
	materialid, _ := this.GetInt("materiaid")
	wxappid := this.GetString("wxappid")
	Content := this.GetString("content")
	Remark := this.GetString("remark")
	materialInfo, _ := models.GetCsTextMaterById(materialid)
	if materialInfo != nil {
		materialInfo.Content = Content
		materialInfo.Remark = Remark
		models.UpdateCsTextMaterById(materialInfo)
	}
	this.Redirect("/material/text/list?wxappid="+wxappid, 302)

}

// 显示微信号所有文字素材列表
func (this *TextMaterialController) WXTextMaterList() {
	this.CheckLogin()
	wxAppId := this.GetString("wxappid")
	MaterList, _ := models.GetAllTextMaterialListWithAppId(wxAppId)
	this.Data["items"] = MaterList
	this.Data["Title"] = "素材管理"
	this.Data["TitleSmall"] = "素材列表"
	this.TplName = "material/text_material_list.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_list.html"
	this.LayoutSections["FooterExt"] = "footerext_list.html"
}

// 分组发送
func (this *TextMaterialController) SendTextGroup() {
	this.CheckLogin()
	materialId, _ := this.GetInt("materialid")
	MaterialInfo, _ := models.GetCsTextMaterById(materialId)
	if MaterialInfo != nil {
		this.Data["groups"], _ = models.GetAllGroupsWithNoParam(MaterialInfo.Wxappid)
	}
	this.Data["item"] = MaterialInfo
	this.Data["Title"] = "发送素材"
	this.Data["TitleSmall"] = "分组发送"
	this.TplName = "material/send_material_group.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_list.html"
	this.LayoutSections["FooterExt"] = "footerext_list.html"
}

// 分组发送提交
func (this *TextMaterialController) SendTextGroupDo() {
	this.CheckLogin()
	wxappid := this.GetString("wxappid")
	groupid := this.GetString("groupid")
	materialid, _ := this.GetInt("materiaid")
	wxInfo, _ := models.GetCsWxAccountByAppId(wxappid)
	materialInfo, _ := models.GetCsTextMaterById(materialid)
	if wxInfo != nil && groupid != "" && materialInfo != nil {
		if wxInfo.Mtype == "1" {
			SendMaterialDo(wxappid, wxInfo.AppSecret, groupid, materialInfo.Content, materialInfo.Mtype, false)
		} else {
			SendMaterialDo(wxappid, wxInfo.AppSecret, groupid, materialInfo.Content, materialInfo.Mtype, true)
		}

	}
	this.Redirect("/material/text/list?wxappid="+wxappid, 302)
}

// 测试发送文字素材
func (this *TextMaterialController) SendTextTest() {
	this.CheckLogin()
	materialId, _ := this.GetInt("materialid")
	MaterialInfo, _ := models.GetCsTextMaterById(materialId)
	this.Data["MaterialInfo"] = MaterialInfo
	this.Data["Title"] = "发送素材"
	this.Data["TitleSmall"] = "发送测试"
	this.TplName = "material/send_material_test.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_list.html"
	this.LayoutSections["FooterExt"] = "footerext_list.html"
}

// 测试发送文字素材提交
func (this *TextMaterialController) SendTextTestDo() {
	this.CheckLogin()
	weixinId := this.GetString("weixinId")
	wxappid := this.GetString("wxappid")
	materialid, _ := this.GetInt("materiaid")
	wxInfo, _ := models.GetCsWxAccountByAppId(wxappid)
	materialInfo, _ := models.GetCsTextMaterById(materialid)
	if wxInfo != nil && materialInfo != nil {
		SendMaterialTest(wxappid, wxInfo.AppSecret, weixinId, materialInfo.Content, materialInfo.Mtype)
	}
	this.Redirect("/material/text/list?wxappid="+wxappid, 302)
}
func check(e error) {
	if e != nil {
		panic(e)
	}
}

// 发送测试内容
func SendMaterialTest(AppId, AppSecret, wxid, content, types string) {
	msg := make(map[string]interface{})
	msg_content := make(map[string]string)
	content = strings.Replace(content, "\r\n", "\n", -1)
	msg_content["content"] = content
	msg["towxname"] = wxid
	msg["msgtype"] = types
	msg["text"] = msg_content
	wechatClient := core.NewClient(core.NewDefaultAccessTokenServer(AppId, AppSecret, nil), nil)

	err := preview.Send(wechatClient, msg)
	if err != nil {
		fmt.Println(err.Error())
	}
}

// 正式发送文字素材
func SendMaterialDo(AppId, AppSecret, GroupId, content, types string, ToAll bool) {
	msg := make(map[string]interface{})
	msg_filter := make(map[string]interface{})
	msg_news := make(map[string]string)
	content = strings.Replace(content, "\r\n", "\n", -1)
	msg_news["content"] = content
	msg_filter["is_to_all"] = ToAll
	if !ToAll {
		msg_filter["group_id"] = GroupId
	}

	msg["filter"] = msg_filter
	msg["text"] = msg_news
	msg["msgtype"] = types
	fmt.Println(msg)

	wechatClient := core.NewClient(core.NewDefaultAccessTokenServer(AppId, AppSecret, nil), nil)

	_, err := mass2group.Send(wechatClient, msg)
	if err != nil {
		fmt.Println(err.Error())
	}
}
