package controllers

import (
	"fmt"
	"time"
	//	"fmt"
	//	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	//	"github.com/astaxie/beego"
	"weixin.nxstory.com/models"
)

type ShowSendStru struct {
	SendMsg   models.CsWxSendmsg
	WxAppName string
}
type SendMsgController struct {
	CommonController
}

func (this *SendMsgController) Get() {
	this.CheckLogin()
	SendList, _ := models.GetAllCsWxSendMsgWithNoParam()
	ShowList := make([]ShowSendStru, 0)
	for _, value := range SendList {
		wxInfo, _ := models.GetCsWxAccountByAppId(value.Wxappid)
		if wxInfo != nil {
			ShowInfo := ShowSendStru{}
			ShowInfo.SendMsg = value
			ShowInfo.WxAppName = wxInfo.Name
			ShowList = append(ShowList, ShowInfo)
		}
	}
	this.Data["items"] = ShowList
	this.Data["Title"] = "发送管理"
	this.Data["TitleSmall"] = "发送列表"
	this.TplName = "sendmsg/index.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_list.html"
	this.LayoutSections["FooterExt"] = "footerext_list.html"
}
func (this *SendMsgController) Add() {
	this.CheckLogin()
	wxList, _ := models.GetAllCsWeixinAccount()
	this.Data["items"] = wxList
	this.Data["Title"] = "发送管理"
	this.Data["TitleSmall"] = "添加任务"
	this.TplName = "sendmsg/add.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_list.html"
	this.LayoutSections["FooterExt"] = "footerext_list.html"
}
func (this *SendMsgController) AddDo() {
	this.CheckLogin()
	AppId := this.GetString("appid")
	SendType, _ := this.GetInt("send_type")
	SendName := this.GetString("name")
	MsgId := this.GetString("msgid")
	SendTime := this.GetString("sendtime")
	fmt.Println("sendtime->", SendTime)
	wxinfo, _ := models.GetCsWxAccountByAppId(AppId)
	if wxinfo != nil {
		SendInfo := models.CsWxSendmsg{}
		SendInfo.Msgid = MsgId
		SendInfo.SendType = SendType
		SendInfo.TaskName = SendName
		SendInfo.Wxappid = AppId
		SendTime, _ := time.Parse("2006-01-02T15:04", SendTime)
		SendInfo.SendTime = SendTime.Format("2006-01-02 15")
		models.AddCsWxSendmsg(&SendInfo)
	}
	this.Redirect("/manager/sengmsg", 302)
}
func (this *SendMsgController) Delete() {
	this.CheckLogin()
	Id, _ := this.GetInt("id")
	models.DeleteCsWxSendmsg(Id)
	this.Redirect("/manager/sengmsg", 302)
}
