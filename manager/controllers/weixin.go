package controllers

import (
	"fmt"
	"strconv"

	"github.com/chanxuehong/wechat.v2/mp/core"
	"github.com/chanxuehong/wechat.v2/mp/material"
	"github.com/chanxuehong/wechat.v2/mp/message/mass/mass2group"
	"github.com/chanxuehong/wechat.v2/mp/user/group"
	"weixin.nxstory.com/models"
)

type WeixinController struct {
	CommonController
}

func GroupDown(AppId, AppSecret string) {

	wechatClient := core.NewClient(core.NewDefaultAccessTokenServer(AppId, AppSecret, nil), nil)
	male_id, female_id, unknown_id := checkgroup(AppId, wechatClient)
	_, err := models.GetCsGroupByAppGroupId(AppId, int(male_id))
	if err != nil && male_id > 0 {
		var _group models.CsGroup
		_group.WxAppId = AppId
		_group.Name = "男性用户"
		_group.Group = int(male_id)
		models.AddCsGroup(&_group)
	}
	_, err = models.GetCsGroupByAppGroupId(AppId, int(female_id))
	if err != nil && female_id > 0 {
		var _group models.CsGroup
		_group.WxAppId = AppId
		_group.Name = "女性用户"
		_group.Group = int(female_id)
		models.AddCsGroup(&_group)
	}
	_, err = models.GetCsGroupByAppGroupId(AppId, int(unknown_id))
	if err != nil && unknown_id > 0 {
		var _group models.CsGroup
		_group.WxAppId = AppId
		_group.Name = "未知用户"
		_group.Group = int(unknown_id)
		models.AddCsGroup(&_group)
	}
	fmt.Println("maleid:", male_id, "femaleid:", female_id, "unknownid:", unknown_id)

}
func checkgroup(AppId string, wechatClient *core.Client) (int64, int64, int64) {
	female_id := int64(0)  //女性用户分类
	male_id := int64(0)    //男性用户分类
	unknown_id := int64(0) //未知用户分类
	wxgroup, err := group.List(wechatClient)
	if err != nil {
		fmt.Println("err->", err.Error())
	}
	fmt.Println("wxgroup->", wxgroup)
	for _, g := range wxgroup {
		if g.Name == "男性用户" {
			male_id = g.Id
		}
		if g.Name == "女性用户" {
			female_id = g.Id
		}
		if g.Name == "未知用户" {
			unknown_id = g.Id
		}
		_, err := models.GetCsGroupByAppGroupId(AppId, int(g.Id))
		if err != nil && unknown_id > 0 {
			var _group models.CsGroup
			_group.WxAppId = AppId
			_group.Name = g.Name
			_group.Group = int(g.Id)
			models.AddCsGroup(&_group)
		}
	}
	if male_id == 0 {
		g, _ := group.Create(wechatClient, "男性用户")
		male_id = g.Id
	}
	if female_id == 0 {
		g, _ := group.Create(wechatClient, "女性用户")
		female_id = g.Id
	}
	if unknown_id == 0 {
		g, _ := group.Create(wechatClient, "未知用户")
		unknown_id = g.Id
	}
	return male_id, female_id, unknown_id
}
func MaterialSend(AppId, AppSecret, MediaId, GroupId string, ToAll bool) {

	msg := make(map[string]interface{})
	msg_filter := make(map[string]interface{})
	msg_news := make(map[string]string)
	msg_news["media_id"] = MediaId
	msg_filter["is_to_all"] = ToAll
	if !ToAll {
		msg_filter["group_id"] = GroupId
	}

	msg["filter"] = msg_filter
	msg["mpnews"] = msg_news
	msg["msgtype"] = "mpnews"
	fmt.Println(msg)

	wechatClient := core.NewClient(core.NewDefaultAccessTokenServer(AppId, AppSecret, nil), nil)

	_, err := mass2group.Send(wechatClient, msg)
	if err != nil {
		fmt.Println(err.Error())
	}
}
func checkMaterial(wxclient *core.Client, AppId string) {
	limit := 20
	from := 0
PROCESS:
	news, err := material.BatchGetNews(wxclient, from, limit)
	if err == nil {
		fmt.Println(news.ItemCount, news.TotalCount)
		for k, v := range news.Items {
			fmt.Println(k, v.MediaId, v.UpdateTime)
			m, err1 := models.GetCsMaterialByAppMId(AppId, v.MediaId)
			fmt.Println(err1)
			if err1 == nil {
				m.Content = v.Content.Articles[0].Title
				models.UpdateCsMaterialById(m)
			} else {
				var _m models.CsMaterial
				_m.WxAppId = AppId
				_m.Mid = v.MediaId
				_m.Mtype = "mpnews"
				_m.Content = v.Content.Articles[0].Title
				models.AddCsMaterial(&_m)
			}
		}
		if news.ItemCount == limit {
			from = from + limit
			fmt.Println("from:", from)
			goto PROCESS
		}
	}
}
func (c *WeixinController) Get() {
	c.CheckLogin()
	query := map[string]string{}

	fields := []string{"Id", "WxAppId", "Name", "Mail"}
	sortby := []string{"Id"}
	order := []string{"desc"}

	items, _ := models.GetAllCsWxAccount(query, fields, sortby, order, 0, 200)

	c.Data["items"] = items
	c.Data["Title"] = "帐号管理"
	c.Data["TitleSmall"] = "列表"
	c.TplName = "weixin/index.html"
	c.Layout = "layout.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["HeaderExt"] = "headerext_list.html"
	c.LayoutSections["FooterExt"] = "footerext_list.html"
}

// 获取某一个微信公众号的所有模版
func (c *WeixinController) GetModels() {
	c.CheckLogin()
	WxIdStr := c.Ctx.Input.Param(":appid")
	modelId := c.GetString("model_id")
	modelName := c.GetString("model_name")
	modelType := c.GetString("model_type")
	c.Data["Title"] = "模版管理"
	c.Data["TitleSmall"] = "模版列表"
	c.Data["weixin_id"] = WxIdStr
	items, _ := models.GetWXModelWithIdAndNameAndType(WxIdStr, modelId, modelName, modelType)
	c.Data["item"] = items
	c.TplName = "weixin/model_list.html"
	c.Layout = "layout.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["HeaderExt"] = "headerext_edit.html"
	c.LayoutSections["FooterExt"] = "footerext_edit.html"
}

// 为某微信公众号添加模版
func (c *WeixinController) AddModel() {
	c.CheckLogin()
	WxIdStr := c.Ctx.Input.Param(":appid")
	c.Data["Title"] = "模版管理"
	c.Data["TitleSmall"] = "添加模版"
	c.Data["weixin_id"] = WxIdStr
	c.Data["type"] = 1
	c.TplName = "weixin/add_model_edit.html"
	c.Layout = "layout.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["HeaderExt"] = "headerext_edit.html"
	c.LayoutSections["FooterExt"] = "footerext_edit.html"
}

func (c *WeixinController) AddModelDo() {
	c.CheckLogin()
	WeixinModel := models.CsWxModel{}
	weixinId := c.Ctx.Input.Param(":appid")
	modelId := c.GetString("model_id", "")
	modelType, _ := c.GetInt("model_type", 0)
	modelName := c.GetString("model_name", "")
	WeixinModel.ModelType = modelType
	WeixinModel.ModelName = modelName
	WeixinModel.WxModelId = modelId
	id, err := models.AddWXModel(&WeixinModel)
	if err != nil {
		fmt.Println("err->", err)
		c.Ctx.WriteString(err.Error())
		c.Redirect("/weixin/model/"+weixinId, 302)
	} else {
		fmt.Println("add success id :", id)
		c.Redirect("/weixin/model/list/"+weixinId, 302)
	}
}

// 修改公众号模版
func (c *WeixinController) UpdateModel() {
	c.CheckLogin()
	WxIdStr := c.Ctx.Input.Param(":appid")
	ModelId := c.Ctx.Input.Param(":model_id")
	model, _ := models.GetWXModelByModelId(WxIdStr, ModelId)
	c.Data["Title"] = "模版管理"
	c.Data["TitleSmall"] = "修改模版"
	c.Data["weixin_id"] = WxIdStr
	c.Data["model_id"] = ModelId
	c.Data["model_name"] = model[0].ModelName
	c.Data["model_type"] = model[0].ModelType
	c.Data["model_no"] = model[0].Id
	c.Data["type"] = 2
	c.TplName = "weixin/add_model_edit.html"
	c.Layout = "layout.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["HeaderExt"] = "headerext_edit.html"
	c.LayoutSections["FooterExt"] = "footerext_edit.html"
}

// 修改公众号模版
func (c *WeixinController) UpdateModelDo() {
	c.CheckLogin()
	WxIdStr := c.Ctx.Input.Param(":appid")
	//	ModelId := c.Ctx.Input.Param(":model_id")
	ModelId := c.GetString("model_id")
	fmt.Println("model->", ModelId)
	id := c.GetString("model_no", "")
	modelType, _ := c.GetInt("model_type", 0)
	modelName := c.GetString("model_name", "")
	idInt, _ := strconv.Atoi(id)
	model, _ := models.GetWXModel(idInt)
	if model != nil {
		if modelName != "" {
			model.ModelName = modelName
		}
		if modelType != 0 {
			model.ModelType = modelType
		}
		if ModelId != "" {
			model.WxModelId = ModelId
		}
		err := models.UpdateWXModelById(model)
		if err == nil {
			c.Redirect("/weixin/model/list/"+WxIdStr, 302)
		} else {
			fmt.Println("err->", err)
		}
	}

	defStr := "/weixin/model/update/" + WxIdStr + "/" + ModelId
	c.Ctx.WriteString("this is update success" + defStr)
	//	c.Redirect(defStr, 302)
}
func (c *WeixinController) Material() {
	c.CheckLogin()
	idStr := c.Ctx.Input.Param(":appid")

	query := map[string]string{}
	if idStr != "" {
		query["wxappid"] = idStr
	}
	fmt.Println("query:", query)
	fields := []string{"Id", "WxAppId", "Mid", "Mtype", "Content"}
	sortby := []string{"Id"}
	order := []string{"desc"}

	items, _ := models.GetAllCsMaterial(query, fields, sortby, order, 0, 200)

	c.Data["items"] = items

	c.Data["wxappid"] = idStr
	c.Data["Title"] = "素材管理"
	c.Data["TitleSmall"] = "素材下载"
	c.TplName = "weixin/material.html"
	c.Layout = "layout.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["HeaderExt"] = "headerext_list.html"
	c.LayoutSections["FooterExt"] = "footerext_list.html"
}
func (c *WeixinController) MaterialDown() {
	c.CheckLogin()
	idStr := c.Ctx.Input.Param(":appid")
	channel, err := models.GetCsWxAccountByAppId(idStr)
	if err == nil {
		wechatClient := core.NewClient(core.NewDefaultAccessTokenServer(channel.WxAppId, channel.AppSecret, nil), nil)
		checkMaterial(wechatClient, channel.WxAppId)

	}
	c.Redirect("/weixin", 302)
}
func (c *WeixinController) GroupDown() {
	c.CheckLogin()
	idStr := c.Ctx.Input.Param(":appid")
	channel, err := models.GetCsWxAccountByAppId(idStr)
	if err == nil {
		GroupDown(channel.WxAppId, channel.AppSecret)
	}
	c.Redirect("/weixin", 302)
}
func (c *WeixinController) SendGroup() {
	c.CheckLogin()
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.Atoi(idStr)
	//取得书信息
	item, _ := models.GetCsMaterialById(id)
	if item != nil {
		c.Data["item"] = item
		query := map[string]string{}
		query["wxappid"] = item.WxAppId
		fields := []string{"Id", "WxAppId", "Name", "Group"}
		sortby := []string{"Id"}
		order := []string{"desc"}
		groups, _ := models.GetAllCsGroup(query, fields, sortby, order, 0, 200)
		c.Data["groups"] = groups
	}

	c.Data["Title"] = "消息发送"
	c.Data["TitleSmall"] = "消息发送"
	c.TplName = "weixin/send.html"
	c.Layout = "layout.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["HeaderExt"] = "headerext_edit.html"
	c.LayoutSections["FooterExt"] = "footerext_edit.html"

}
func (c *WeixinController) SendGroupDo() {
	c.CheckLogin()
	idStr := c.Ctx.Input.Param(":id")
	groupid := c.GetString("groupid")
	id, _ := strconv.Atoi(idStr)
	//取得书信息
	item, err := models.GetCsMaterialById(id)
	if err == nil && groupid != "" {
		channel, _ := models.GetCsWxAccountByAppId(item.WxAppId)

		fmt.Println(channel.WxAppId, channel.AppSecret, item.Mid, groupid, false)
		MaterialSend(channel.WxAppId, channel.AppSecret, item.Mid, groupid, false)

	}
	c.Redirect("/weixin", 302)

}
func (c *WeixinController) SendAll() {
	c.CheckLogin()
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.Atoi(idStr)
	//取得书信息
	item, _ := models.GetCsMaterialById(id)
	if item != nil {
		c.Data["item"] = item

	}

	c.Data["Title"] = "消息发送"
	c.Data["TitleSmall"] = "消息发送"
	c.TplName = "weixin/send.html"
	c.Layout = "layout.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["HeaderExt"] = "headerext_edit.html"
	c.LayoutSections["FooterExt"] = "footerext_edit.html"

}

func (c *WeixinController) SendAllDo() {
	c.CheckLogin()
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.Atoi(idStr)
	//取得书信息
	item, err := models.GetCsMaterialById(id)
	if err == nil {
		channel, _ := models.GetCsWxAccountByAppId(item.WxAppId)

		fmt.Println(channel.WxAppId, channel.AppSecret, item.Mid, "", true)
		MaterialSend(channel.WxAppId, channel.AppSecret, item.Mid, "", true)

	}
	c.Redirect("/weixin", 302)

}

// 添加网站唯一ID
func (this *WeixinController) AddOnlyId() {
	this.CheckLogin()
	WxAppId := this.GetString("wxappid")
	this.Data["wxappid"] = WxAppId
	this.Data["Title"] = "消息发送"
	this.Data["TitleSmall"] = "消息发送"
	this.TplName = "weixin/add_only_id.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_edit.html"
	this.LayoutSections["FooterExt"] = "footerext_edit.html"
}

// 添加站唯一ID提交
func (this *WeixinController) AddOnlyIdDo() {
	wxAppID := this.GetString("wx_appid")
	OnlyId := this.GetString("only_id")
	wxinfo, _ := models.GetCsWxAccountByAppId(wxAppID)
	if wxinfo != nil {
		wxinfo.OriginalId = OnlyId
		models.UpdateCsWxAccountById(wxinfo)
	}
	this.Redirect("/weixin", 302)
}
