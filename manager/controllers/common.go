package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/astaxie/beego"
	"weixin.nxstory.com/manager/utils"
	"weixin.nxstory.com/models"
)

// 请求token地址
var GetWXTokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s"
var CreateMenu = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token="

type CommonController struct {
	beego.Controller
}
type CpOss struct {
	Host      string
	Key       string
	Pass      string
	Bucket    string
	RealmName string
}

func (this *CpOss) Init() {
	this.Host = beego.AppConfig.String("alioss::host")
	this.Key = beego.AppConfig.String("alioss::key")
	this.Pass = beego.AppConfig.String("alioss::pass")
	this.Bucket = beego.AppConfig.String("alioss::bucket")
	this.RealmName = beego.AppConfig.String("fileurl")
}

// 请求Token返回
type AccessTokenResponse struct {
	Access_token string `json:"access_token"`
	Expires_in   int    `json:"expires_in"`
}
type TokenStr struct {
	WXToken     string `json:"wx_token"`
	WXTokenTime int64  `json:"wx_time"`
}

// get token from redis
func GetTokenFromRedis(appid, appsecret string) (token string, err error) {
	apptoken := models.GetCache(appid)
	if apptoken != nil {
		return string(apptoken), err
	} else {
		token, err := GetTokenFromHttp(appid, appsecret)
		if err != nil {
			return "", err
		} else {
			models.PutCache(appid, []byte(token), 7000)
			return token, nil
		}
	}
}

// 获取Token（通过请求）
func GetTokenFromHttp(appid, appSecret string) (appToken string, err error) {
	Url := fmt.Sprintf(GetWXTokenUrl, appid, appSecret)
	Access := AccessTokenResponse{}
	fmt.Println("Url->", Url)
	res, err := http.Get(Url)
	resStr, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err == nil {
		err = json.Unmarshal(resStr, &Access)
		if err != nil {
			fmt.Println("err1->", err.Error())
			return "", err
		}
	} else {
		fmt.Println("err2->", err.Error())
		return "", err
	}
	SetSessToekn := TokenStr{}
	SetSessToekn.WXToken = Access.Access_token
	SetSessToekn.WXTokenTime = time.Now().Unix()
	return Access.Access_token, nil
}

// 存储图片
func (this *CommonController) UploadImgtooss(ImgKey string, id int, SaveType int) string {
	// SaveType 1:课程封面 2：教师头像 3：课时文件 4 推荐图片
	var fileUrl string
	var rootUrl string
	var secoUrl string
	switch SaveType {
	case 1:
		rootUrl = "cover"
		secoUrl = "/lessoncover/"
	case 2:
		rootUrl = "cover"
		secoUrl = "/teacherimg/"
	case 3:
		rootUrl = "chapter"
		secoUrl = "/chapterfile/"
	case 4:
		rootUrl = "promote"
		secoUrl = "/"
	}
	fileUrl = rootUrl + secoUrl
	client, err := oss.New(beego.AppConfig.String("alioss::host"), beego.AppConfig.String("alioss::key"), beego.AppConfig.String("alioss::pass"))
	if err != nil {
		fmt.Println("fail2:", err.Error())
		return ""
	}
	bucket, err := client.Bucket(beego.AppConfig.String("alioss::bucket"))

	if err != nil {
		return ""
	}

	aa, hh, err := this.GetFile(ImgKey)
	if err != nil {
		return ""
	}
	cover := strconv.FormatInt(time.Now().UnixNano(), 10) + strconv.Itoa(id) + utils.GetRandomString(8) + utils.SubFileName(hh.Filename)
	err = bucket.PutObject(fileUrl+cover, aa)
	aa.Close()
	if err != nil {
		fmt.Println("fail:", err.Error())
		return ""
	}
	fmt.Println("cover->", secoUrl+cover)
	return secoUrl + cover
}
func (c *CommonController) uploadpictooss(cover string) string {
	//oss-cn-beijing-internal.aliyuncs.com
	client, err := oss.New(beego.AppConfig.String("alioss::host"), beego.AppConfig.String("alioss::key"), beego.AppConfig.String("alioss::pass"))
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}

	bucket, err := client.Bucket(beego.AppConfig.String("alioss::bucket"))
	//bucket, err := client.Bucket("booktxt")
	if err != nil {
		fmt.Println(err.Error())

		return ""
	}

	aa, _, err := c.GetFile("cover")
	if err != nil {
		fmt.Println(err.Error())

		return ""
	}
	err = bucket.PutObject(cover, aa)
	aa.Close()
	if err != nil {
		fmt.Println("fail:", err.Error())
		return ""
	}
	return cover

}
func (c *CommonController) CheckLogin() *models.CsAdminUser {
	fmt.Println(beego.AppConfig.String("admin_user_id"))
	userid := c.GetSession(beego.AppConfig.String("admin_user_id"))
	if userid != nil {
		useridInt := userid.(int)
		userinfo, _ := models.GetAdminUserWithUseId(useridInt)
		if userinfo != nil {
			return userinfo
		} else {
			fmt.Println("user is null")
			c.Redirect("/user/login", 302)
			return nil
		}
	} else {
		fmt.Println("userid is null")
		c.Redirect("/user/login", 302)
		return nil
	}
}
