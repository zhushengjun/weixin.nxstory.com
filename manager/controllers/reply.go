package controllers

import (
	"fmt"

	"weixin.nxstory.com/models"
)

type ReplyManagerController struct {
	CommonController
}
type ReplyInfo struct {
	Id           int    `json:"id"`
	AppName      string `json:"appname"`
	AppId        string `json:"appid"`
	KeyWord      string `json:"keyword"`
	ReplyContent string `json:"replycontent"`
}

// 关键字回复 列表
func (this *ReplyManagerController) GetList() {
	this.CheckLogin()
	appid := this.GetString("appid")
	keyword := this.GetString("keyword")
	KeyWordList, _ := models.GetAllKeyReplyWithAppid(keyword, appid)
	showList := this.GetAllParamWithKeyList(KeyWordList)
	if appid != "" {
		this.Data["appid"] = appid
	}
	if keyword != "" {
		this.Data["keyword"] = keyword
	}
	this.Data["items"] = showList
	this.Data["Title"] = "关键字回复"
	this.Data["TitleSmall"] = "列表"
	this.TplName = "reply/reply_index.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_main.html"
	this.LayoutSections["FooterExt"] = "footerext_main.html"
}

// 关键字回复 添加
func (this *ReplyManagerController) AddReply() {
	this.CheckLogin()
	accountList, _ := models.GetAllCsWeixinAccount()
	this.Data["id"] = 0
	this.Data["items"] = accountList
	this.Data["Title"] = "关键字回复"
	this.Data["TitleSmall"] = "添加"
	this.TplName = "reply/reply_add.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_main.html"
	this.LayoutSections["FooterExt"] = "footerext_main.html"
}

// 关键字回复 添加提交
func (this *ReplyManagerController) AddReplyDo() {
	this.CheckLogin()
	AppId := this.GetString("appid")
	KeyWord := this.GetString("reply_keyword")
	ReplyContent := this.GetString("reply_content")
	wxinfo, _ := models.GetCsWxAccountByAppId(AppId)
	if wxinfo != nil && KeyWord != "" && ReplyContent != "" {
		replyInfo := models.CsKeywordsOperate{}
		replyInfo.Keyword = KeyWord
		replyInfo.Resultword = ReplyContent
		replyInfo.WxAppId = wxinfo.WxAppId
		_, err := models.AddCsKeywordsOperate(&replyInfo)
		if err != nil {
			fmt.Println("err->", err.Error())
		}
	}
	this.Redirect("/manager/replymanager", 302)
}

// 关键字回复 编辑
func (this *ReplyManagerController) EditReply() {
	this.CheckLogin()
	KeyId, _ := this.GetInt("id")
	keywordInfo, _ := models.GetCsKeywordsOperateById(KeyId)
	if keywordInfo != nil {
		wxinfo, _ := models.GetCsWxAccountByAppId(keywordInfo.WxAppId)
		if wxinfo != nil {
			this.Data["name"] = wxinfo.Name
		}
	}
	this.Data["info"] = keywordInfo
	this.Data["Title"] = "关键字回复"
	this.Data["TitleSmall"] = "编辑"
	this.TplName = "reply/reply_edit.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_main.html"
	this.LayoutSections["FooterExt"] = "footerext_main.html"
}

// 关键字回复 编辑提交
func (this *ReplyManagerController) EditReplyDo() {
	id, _ := this.GetInt("id")
	appId := this.GetString("appid")
	keyword := this.GetString("reply_keyword")
	content := this.GetString("reply_content")
	keywordInfo, _ := models.GetKeywordWithIdAndAppid(id, appId)
	if keywordInfo != nil {
		keywordInfo.Keyword = keyword
		keywordInfo.Resultword = content
		models.UpdateCsKeywordsOperateById(keywordInfo)
	}
	this.Redirect("/manager/replymanager", 302)
}

// 关键字回复 删除
func (this *ReplyManagerController) DeleteReplyDo() {
	id, _ := this.GetInt("id")
	keywordinfo, _ := models.GetCsKeywordsOperateById(id)
	if keywordinfo != nil {
		models.DeleteCsKeywordsOperate(id)
	}
	this.Redirect("/manager/replymanager", 302)
}
func (this *ReplyManagerController) GetAllParamWithKeyList(keywordList []models.CsKeywordsOperate) (replyList []ReplyInfo) {
	for _, value := range keywordList {
		wxinfo, _ := models.GetCsWxAccountByAppId(value.WxAppId)
		if wxinfo != nil {
			replyInfo := ReplyInfo{}
			replyInfo.AppId = wxinfo.WxAppId
			replyInfo.AppName = wxinfo.Name
			replyInfo.Id = value.Id
			replyInfo.KeyWord = value.Keyword
			replyInfo.ReplyContent = value.Resultword
			replyList = append(replyList, replyInfo)
		}
	}
	return replyList
}
