package controllers

import (
	"fmt"
	"time"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/astaxie/beego"
	"weixin.nxstory.com/models"
)

type MessageController struct {
	CommonController
}

func (this *MessageController) Get() {
	this.CheckLogin()
	msgType, _ := this.GetInt("msg_type")
	msgName := this.GetString("msg_name")
	msgId, _ := this.GetInt("msg_id")
	fmt.Println("msgType->", msgType, "msg_name->", msgName, "msgId->", msgId)
	MatterList, _ := models.GetAllCsMsgMatterByNoParam(msgId, msgName, msgType)
	this.Data["Title"] = "消息管理"
	this.Data["TitleSmall"] = "消息列表"
	this.Data["msg_type"] = msgType
	this.Data["msg_id"] = msgId
	this.Data["items"] = MatterList
	this.TplName = "matter/index.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_list.html"
	this.LayoutSections["FooterExt"] = "footerext_list.html"
}
func (this *MessageController) AddTestMessage() {
	this.CheckLogin()
	this.Data["Title"] = "消息管理"
	this.Data["TitleSmall"] = "添加文字消息"
	this.TplName = "matter/add_text.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_list.html"
	this.LayoutSections["FooterExt"] = "footerext_list.html"
}
func (this *MessageController) AddTestMessageDo() {
	this.CheckLogin()
	Name := this.GetString("name")
	Content := this.GetString("text_message")
	MessageInfo := models.CsWxMsgMatter{}
	MessageInfo.MsgName = Name
	MessageInfo.Content = Content
	MessageInfo.MsgType = 1
	models.AddCsWxMsgMatter(&MessageInfo)
	this.Redirect("/manager/message", 302)
}
func (this *MessageController) EditTextMessage() {
	this.CheckLogin()
	Id, _ := this.GetInt("id")
	TextMessage, _ := models.GetCsWxMsgMatterById(Id)
	this.Data["msgInfo"] = TextMessage
	this.Data["Title"] = "消息管理"
	this.Data["TitleSmall"] = "修改文字消息"
	this.TplName = "matter/update_text.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_list.html"
	this.LayoutSections["FooterExt"] = "footerext_list.html"
}
func (this *MessageController) EditTextMessageDo() {
	this.CheckLogin()
	MsgId, _ := this.GetInt("msgId")
	MsgName := this.GetString("name")
	Content := this.GetString("text_message")
	TextMsgInfo, _ := models.GetCsWxMsgMatterById(MsgId)
	if TextMsgInfo != nil {
		TextMsgInfo.Content = Content
		TextMsgInfo.MsgName = MsgName
		models.UpdateCsWxMsgMatterById(TextMsgInfo)
	}
	this.Redirect("/manager/message", 302)
}
func (this *MessageController) AddImgMessage() {
	this.CheckLogin()
	this.Data["Title"] = "消息管理"
	this.Data["TitleSmall"] = "添加图文消息"
	this.TplName = "matter/add_img.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_list.html"
	this.LayoutSections["FooterExt"] = "footerext_list.html"
}

func (this *MessageController) AddImgMesssageDo() {
	this.CheckLogin()
	Name := this.GetString("name")
	Title := this.GetString("title")
	Descrip := this.GetString("descrip")
	Url := this.GetString("url")
	ImgMessage := models.CsWxMsgMatter{}
	ImgMessage.MsgName = Name
	ImgMessage.Title = Title
	ImgMessage.Descrip = Descrip
	ImgMessage.Url = Url
	ImgMessage.MsgType = 2
	MsgId, _ := models.AddCsWxMsgMatter(&ImgMessage)
	cover := fmt.Sprintf("%s/%d.jpg", time.Now().Format("2006/1/2"), MsgId)
	CoverStr := uploadfiletooss(this, cover)
	if CoverStr != "" {
		ImgInfo, _ := models.GetCsWxMsgMatterById(int(MsgId))
		ImgInfo.Picurl = CoverStr
		models.UpdateCsWxMsgMatterById(ImgInfo)
	}
	this.Redirect("/manager/message", 302)
}
func (this *MessageController) EditImgMessage() {
	this.CheckLogin()
	Id, _ := this.GetInt("id")
	ImgMessage, _ := models.GetCsWxMsgMatterById(Id)
	Res := beego.AppConfig.String("resurl")
	this.Data["ResUrl"] = Res
	this.Data["msgInfo"] = ImgMessage
	this.Data["Title"] = "消息管理"
	this.Data["TitleSmall"] = "修改图文消息"
	this.TplName = "matter/update_img.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_list.html"
	this.LayoutSections["FooterExt"] = "footerext_list.html"
}
func (this *MessageController) EditImgMessageDo() {
	this.CheckLogin()
	MsgId, _ := this.GetInt("msgId")
	fmt.Println("msgid->", MsgId)
	ImgMsgInfo, _ := models.GetCsWxMsgMatterById(MsgId)
	if ImgMsgInfo != nil {
		Name := this.GetString("name")
		Title := this.GetString("title")
		Descrip := this.GetString("descrip")
		Url := this.GetString("url")
		ImgMsgInfo.MsgName = Name
		ImgMsgInfo.Title = Title
		ImgMsgInfo.Descrip = Descrip
		ImgMsgInfo.Url = Url
		cover := ImgMsgInfo.Picurl
		if ImgMsgInfo.Picurl == "" {
			cover = fmt.Sprintf("%s/%d.jpg", time.Now().Format("2006/1/2"), ImgMsgInfo.Id)
		}
		retcover := uploadfiletooss(this, cover)
		if retcover != "" {
			ImgMsgInfo.Picurl = retcover
		}
		models.UpdateCsWxMsgMatterById(ImgMsgInfo)
	}
	this.Redirect("/manager/message", 302)
}

func uploadfiletooss(c *MessageController, cover string) string {
	//oss-cn-beijing-internal.aliyuncs.com
	fmt.Println("accesss upload file")
	client, err := oss.New(beego.AppConfig.String("alioss::host"), beego.AppConfig.String("alioss::key"), beego.AppConfig.String("alioss::pass"))
	if err != nil {
		fmt.Println("get client error", err.Error())
		return ""
	}

	bucket, err := client.Bucket(beego.AppConfig.String("alioss::bucket"))
	//bucket, err := client.Bucket("booktxt")
	if err != nil {
		fmt.Println("get bucket error", err.Error())
		return ""
	}

	aa, _, err := c.GetFile("img")
	if err != nil {
		fmt.Println("get File error", err.Error())
		return ""
	}
	fmt.Println("Cover->", cover)
	err = bucket.PutObject("wximg/"+cover, aa)
	aa.Close()
	if err != nil {
		fmt.Println("fail:", err.Error())
		return ""
	}
	return cover
}
func (this *MessageController) DeleteMessage() {
	this.CheckLogin()
	Id, _ := this.GetInt("id")
	models.DeleteCsTextMater(Id)
	this.Redirect("/manager/message", 302)
}
