package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"weixin.nxstory.com/models"
)

type MenuManagerController struct {
	CommonController
}
type MenuManagerShowStru struct {
	Wxaccount models.CsWxAccount
	IsFull    int // 0 没满 1 已满
}

func (this *MenuManagerController) Get() {
	this.CheckLogin()
	fmt.Println("menu")
	accountList, _ := models.GetAllCsWeixinAccount()
	Items := make([]MenuManagerShowStru, 0)
	for _, value := range accountList {
		ShowMenu := MenuManagerShowStru{}
		list, _ := models.GetMenuListWithAppidAndPid(value.WxAppId, 0)
		if len(list) < 3 {
			ShowMenu.IsFull = 0
		} else {
			ShowMenu.IsFull = 1
		}
		ShowMenu.Wxaccount = value
		Items = append(Items, ShowMenu)
	}
	this.Data["items"] = Items
	this.Data["Title"] = "菜单管理"
	this.Data["TitleSmall"] = "公众号列表"
	this.TplName = "menu/menu_index.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_main.html"
	this.LayoutSections["FooterExt"] = "footerext_main.html"
}

// 查看菜单
func (this *MenuManagerController) ListMenu() {
	this.CheckLogin()
	appid := this.GetString("appid")
	Pid, _ := this.GetInt("pid")
	menuList, _ := models.GetMenuListWithAppidAndPid(appid, Pid)
	this.Data["items"] = menuList
	this.Data["Title"] = "菜单管理"
	this.Data["TitleSmall"] = "菜单列表"
	this.TplName = "menu/menu_list.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_main.html"
	this.LayoutSections["FooterExt"] = "footerext_main.html"
}

// 编辑菜单
func (this *MenuManagerController) EditMenu() {
	this.CheckLogin()
	menuId, _ := this.GetInt("id")
	menuInfo, _ := models.GetCsWxMenuById(menuId)
	if menuInfo != nil {
		this.Data["info"] = menuInfo
		wxinfo, _ := models.GetCsWxAccountByAppId(menuInfo.WxAppId)
		if wxinfo != nil {
			this.Data["wxinfo"] = wxinfo
		}
	}
	this.Data["Title"] = "菜单管理"
	this.Data["TitleSmall"] = "菜单编辑"
	this.TplName = "menu/menu_edit.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_main.html"
	this.LayoutSections["FooterExt"] = "footerext_main.html"
}

// 编辑菜单提交
func (this *MenuManagerController) EditMenuDo() {
	MenuId, _ := this.GetInt("menu_id")
	Appid := this.GetString("appid")
	MenuName := this.GetString("menu_name")
	MenuType, _ := this.GetInt("menu_type")
	MenuUrl := this.GetString("menu_url")
	MenuKey := this.GetString("menu_key")
	Order, _ := this.GetInt("menu_order")
	MenuContent := this.GetString("menu_content")
	MenuInfo, _ := models.GetCsWxMenuById(MenuId)
	if MenuInfo != nil {
		MenuInfo.Name = MenuName
		MenuInfo.OrderId = Order
		MenuInfo.Type = MenuType
		if MenuType == 1 {
			MenuInfo.Tourl = MenuUrl
			MenuInfo.MenuKey = ""
			MenuInfo.ResContent = ""
		} else if MenuType == 3 {
			MenuInfo.Tourl = ""
			MenuInfo.MenuKey = MenuKey
			MenuInfo.ResContent = MenuContent
		}
		models.UpdateCsWxMenuById(MenuInfo)
	}
	this.Redirect("/manager/menu/list?appid="+Appid, 302)
}

// 删除菜单提交
func (this *MenuManagerController) DeleteMenu() {
	MenuId, _ := this.GetInt("id")
	appid := this.GetString("appid")
	models.DeleteCsWxMenu(MenuId)
	models.DeleteMenuWithPid(MenuId)
	this.Redirect("/manager/menu/list?appid="+appid, 302)
}

// 管理二级菜单
func (this *MenuManagerController) ListSubMenu() {
	menuId, _ := this.GetInt("id")
	appid := this.GetString("appid")
	menuList, _ := models.GetMenuListWithAppidAndPid(appid, menuId)
	this.Data["items"] = menuList
	this.Data["Title"] = "菜单管理"
	this.Data["TitleSmall"] = "二级菜单列表"
	this.TplName = "menu/menu_list.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_main.html"
	this.LayoutSections["FooterExt"] = "footerext_main.html"
}

// 添加二级菜单
func (this *MenuManagerController) AddSubMenu() {
	menuId, _ := this.GetInt("id")
	appid := this.GetString("appid")
	wxinfo, _ := models.GetCsWxAccountByAppId(appid)
	if wxinfo != nil {
		this.Data["wxinfo"] = wxinfo
	}
	this.Data["menuid"] = menuId
	this.Data["appid"] = appid
	this.Data["Title"] = "菜单管理"
	this.Data["TitleSmall"] = "添加二级菜单"
	this.TplName = "menu/menu_edit.html"
	this.Layout = "layout.html"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["HeaderExt"] = "headerext_main.html"
	this.LayoutSections["FooterExt"] = "footerext_main.html"
}

// 添加菜单提交
func (this *MenuManagerController) AddSubMenuDo() {
	pid, _ := this.GetInt("menuid")
	appid := this.GetString("appid")
	MenuName := this.GetString("menu_name")
	MenuType, _ := this.GetInt("menu_type")
	MenuUrl := this.GetString("menu_url")
	MenuKey := this.GetString("menu_key")
	Order, _ := this.GetInt("menu_order")
	MenuContent := this.GetString("menu_content")
	MenuInfo := models.CsWxMenu{}
	MenuInfo.WxAppId = appid
	MenuInfo.Pid = pid
	MenuInfo.Name = MenuName
	MenuInfo.OrderId = Order
	MenuInfo.Type = MenuType
	MenuInfo.CreateTime = time.Now()
	if MenuType == 1 {
		MenuInfo.Tourl = MenuUrl
		MenuInfo.MenuKey = ""
		MenuInfo.ResContent = ""
	} else if MenuType == 3 {
		MenuInfo.Tourl = ""
		MenuInfo.MenuKey = MenuKey
		MenuInfo.ResContent = MenuContent
	} else {
		MenuInfo.Tourl = ""
		MenuInfo.MenuKey = ""
		MenuInfo.ResContent = ""
	}
	_, err := models.AddCsWxMenu(&MenuInfo)
	if err != nil {
		fmt.Println("err->", err.Error())
	}
	if pid == 0 {
		this.Redirect("/manager/menu", 302)
	} else {
		this.Redirect("/manager/menu/list?appid="+appid, 302)
	}

}

type MenuType struct {
	Name    string    `json:"name"`       // 菜单显示名称
	Types   string    `json:"type"`       // 菜单类型
	Key     string    `json:"key"`        // 如果是事件 菜单的key
	Url     string    `json:"url"`        // 如果是点击跳转 跳转的url
	SubMenu []SubMenu `json:"sub_button"` // 如果是子菜单，子菜单的内容
}
type SubMenu struct {
	Name  string `json:"name"` // 菜单显示名称
	Types string `json:"type"` // 菜单类型
	Key   string `json:"key"`  // 如果是事件 菜单的key
	Url   string `json:"url"`  // 如果是点击跳转 跳转的url
}

// 更新菜单提交
func (this *MenuManagerController) UpdateMenuDo() {
	appid := this.GetString("appid")
	wxinfo, _ := models.GetCsWxAccountByAppId(appid)
	menuList, _ := models.GetAllMenuByAppid(appid, 0)
	ReqMap := make([]MenuType, 0)
	if wxinfo != nil && menuList != nil {
		for _, value := range menuList {
			menu := MenuType{}
			menu.Name = value.Name
			if value.Type == 1 { // url 跳转
				menu.Types = "view"
				menu.Url = value.Tourl
			} else if value.Type == 2 { // 二级菜单
				subMenu, _ := models.GetAllMenuByAppid(appid, value.Id)
				subMenuList := make([]SubMenu, 0)
				if len(subMenu) <= 6 || len(subMenu) > 0 {
					fmt.Println("111")
					for _, subvalue := range subMenu {
						subMenuMap := SubMenu{}
						subMenuMap.Name = subvalue.Name
						if subvalue.Type == 1 { //跳转
							subMenuMap.Types = "view"
							subMenuMap.Url = subvalue.Tourl
						} else if subvalue.Type == 3 { //事件获取
							subMenuMap.Types = "click"
							subMenuMap.Key = subvalue.MenuKey
						}
						subMenuList = append(subMenuList, subMenuMap)
					}
				} else {
					fmt.Println("222")
					for i := 0; i < len(subMenu); i++ {
						subMenuMap := SubMenu{}
						subMenuMap.Name = subMenu[i].Name
						if subMenu[i].Type == 1 { //跳转
							subMenuMap.Types = "view"
							subMenuMap.Url = subMenu[i].Tourl
						} else if subMenu[i].Type == 3 { //事件获取
							subMenuMap.Types = "click"
							subMenuMap.Key = subMenu[i].MenuKey
						}
						subMenuList = append(subMenuList, subMenuMap)
					}
				}
				menu.SubMenu = subMenuList
			} else if value.Type == 3 { // 事件获取
				menu.Types = "click"
				menu.Key = value.MenuKey
			}
			ReqMap = append(ReqMap, menu)
		}
		Maps := make(map[string]interface{})
		Maps["button"] = ReqMap
		MapJson, _ := json.Marshal(Maps)
		fmt.Println("subMenuList->", string(MapJson))
		AccessToken, _ := GetTokenFromRedis(wxinfo.WxAppId, wxinfo.AppSecret)
		Url := CreateMenu + AccessToken
		resp, err := http.Post(Url, "application/x-www-form-urlencoded", strings.NewReader(string(MapJson)))
		if err != nil {
			fmt.Println(err)
		}

		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			// handle error
		}
		fmt.Println(string(body))
		//		req := httplib.Post(Url)
		//		req.Header("Content-Type", "text/html")
		//		req.Header("charset", "UTF-8")
		//		req.Param("button", string(MapStr))
		//		res, _ := req.String()
		//		fmt.Println("String->", string(res))
	}
	this.Redirect("/manager/menu", 302)
}
