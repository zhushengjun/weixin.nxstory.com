package main

import (
	"fmt"
	"net/url"

	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/astaxie/beego/session/redis"
	_ "github.com/go-sql-driver/mysql"
	_ "weixin.nxstory.com/manager/routers"
)

func init() {
	//设置业务数据库
	orm.RegisterDataBase("default", "mysql", fmt.Sprintf(beego.AppConfig.String("db::slave_operation"), url.QueryEscape("Local")), 3, 15)
	orm.RegisterDataBase("m_operation", "mysql", fmt.Sprintf(beego.AppConfig.String("db::master_operation"), url.QueryEscape("Local")), 3, 15)
	orm.DefaultTimeLoc, _ = time.LoadLocation("Local")
}
func main() {
	beego.SetStaticPath("/assets", "assets")
	beego.SetStaticPath("/static", "static")
	beego.AddFuncMap("addone", addone)
	beego.Run()
}
func addone(in int) (out int) {
	out = in + 1
	return
}
