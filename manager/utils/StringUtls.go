package utils

import (
	"crypto/md5"
	"encoding/hex"
	"math/rand"
	"strings"
	"time"
)

func Md5Str(OperStr string) (MD5Str string) {
	h := md5.New()
	h.Write([]byte(OperStr))
	cipherStr := h.Sum(nil)
	return hex.EncodeToString(cipherStr)
}
func SubIpString(IpString string) (ip string) {
	IndexEnd := strings.Index(IpString, ":")
	return IpString[0:IndexEnd]
}
func SubSpace(OldStr string) (NewStr string) {
	NewStr = OldStr
	NewStr = strings.Replace(NewStr, " ", "", -1)
	NewStr = strings.Replace(NewStr, "　", "", -1)
	return NewStr
}
func SubFileName(oldStr string) (suffix string) {
	IndexEnd := strings.LastIndex(oldStr, ".")
	return oldStr[IndexEnd:len(oldStr)]
}
func GetRandomString(l int) string {
	str := "0123456789abcdefghijklmnopqrstuvwxyz"
	bytes := []byte(str)

	result := []byte{}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < l; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}
