package main

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strings"

	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/astaxie/beego/session/redis"
	_ "github.com/go-sql-driver/mysql"
	"weixin.nxstory.com/models"
)

const (
	GetWXTokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s"
	SengMsgUrl    = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token="
)

var ResUrl = beego.AppConfig.String("resurl")

type SendRes struct {
	Code  int    `json:"errcode"`
	Msg   string `json:"errmsg"`
	MsgId int    `json:"msgid"`
}

var AppToken string

// 请求Token返回
type AccessTokenResponse struct {
	Access_token string `json:"access_token"`
	Expires_in   int    `json:"expires_in"`
}
type TokenStr struct {
	WXToken     string `json:"wx_token"`
	WXTokenTime int64  `json:"wx_time"`
}
type SendCommon struct {
	ToUser  string `json:"touser"`
	MsgType string `json:"msgtype"`
}
type SendText struct {
	SendCommon
	Text TextMsgInfo `json:"text"`
}
type TextMsgInfo struct {
	Content string `json:"content"`
}
type SendImg struct {
	SendCommon
	News ImgStru `json:"news"`
}
type ImgStru struct {
	Articles []ImgMsgInfo `json:"articles"`
}
type ImgMsgInfo struct {
	Title   string `json:"title"`
	Descrip string `json:"description"`
	SkipUrl string `json:"url"`
	PicUrl  string `json:"picurl"`
}

func init() {
	//设置业务数据库
	orm.RegisterDataBase("default", "mysql", fmt.Sprintf(beego.AppConfig.String("db::slave_operation"), url.QueryEscape("Asia/Shanghai")), 3, 15)
	orm.RegisterDataBase("m_operation", "mysql", fmt.Sprintf(beego.AppConfig.String("db::master_operation"), url.QueryEscape("Asia/Shanghai")), 3, 15)
	orm.DefaultTimeLoc, _ = time.LoadLocation("Asia/Shanghai")
}
func main() {
	NowTime := time.Now().Format("2006-01-02 15")
	fmt.Println("now time->", NowTime)
	SendList, _ := models.GetAllWxSendMsgByTime(NowTime)
	fmt.Println("send list->", SendList)
	if len(SendList) > 0 {
		for _, value := range SendList {
			if value.SendType == 1 {
				SendTextMsg(value.Wxappid, value.Msgid)
			} else {
				SendImgMsg(value.Wxappid, value.Msgid)
			}
		}
	}
}

// 发送文本消息
func SendTextMsg(wxappid string, msgid string) {
	wxInfo, _ := models.GetCsWxAccountByAppId(wxappid)
	msgIdList := strings.Split(msgid, ",")
	MsgId, _ := strconv.Atoi(msgIdList[0])
	msgInfo, _ := models.GetCsWxMsgMatterById(MsgId)
	if wxInfo != nil && msgInfo != nil {
		newTime := time.Now()
		m, _ := time.ParseDuration("-48h")
		LastTime := newTime.Add(m)
		StartTime := LastTime.Format("2006-01-02 15:04:05")
		UserList, _ := models.GetAllUserWithTimeAndAppid(wxInfo.WxAppId, StartTime)
		if len(UserList) > 0 {
			fmt.Println("user list is not null")
			token, _ := GetTokenFromRedis(wxInfo.WxAppId, wxInfo.AppSecret)
			for _, value := range UserList {
				fmt.Println("value openid->", value.Openid, "        value name->", value.Nickname)
				SendMsgDo(token, value.Openid, msgInfo.Content)
			}
		}
	} else {
		fmt.Println("wxinfo is null or msg info is null")
	}
}

// get token from redis
func GetTokenFromRedis(appid, appsecret string) (token string, err error) {
	apptoken := models.GetCache(appid)
	if apptoken != nil {
		return string(apptoken), err
	} else {
		token, err := GetTokenFromHttp(appid, appsecret)
		if err != nil {
			return "", err
		} else {
			models.PutCache(appid, []byte(token), 7000)
			return token, nil
		}
	}
}

// 获取Token（通过请求）
func GetTokenFromHttp(appid, appSecret string) (appToken string, err error) {
	Url := fmt.Sprintf(GetWXTokenUrl, appid, appSecret)
	Access := AccessTokenResponse{}
	fmt.Println("Url->", Url)
	res, err := http.Get(Url)
	resStr, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err == nil {
		err = json.Unmarshal(resStr, &Access)
		if err != nil {
			fmt.Println("err1->", err.Error())
			return "", err
		}
	} else {
		fmt.Println("err2->", err.Error())
		return "", err
	}
	SetSessToekn := TokenStr{}
	SetSessToekn.WXToken = Access.Access_token
	SetSessToekn.WXTokenTime = time.Now().Unix()
	return Access.Access_token, nil
}

// 发送图文消息
func SendImgMsg(wxappid string, msgid string) {
	fmt.Println("login send img")
	wxInfo, _ := models.GetCsWxAccountByAppId(wxappid)
	MsgMatterList := make([]models.CsWxMsgMatter, 0)
	msgIdList := strings.Split(msgid, ",")
	for _, value := range msgIdList {
		msgId, _ := strconv.Atoi(value)
		fmt.Println("msgid->", msgId)
		msgMatter, _ := models.GetCsWxMsgMatterById(msgId)
		if msgMatter != nil {
			MsgMatterList = append(MsgMatterList, *msgMatter)
		}
	}
	if wxInfo != nil && len(MsgMatterList) > 0 && len(MsgMatterList) < 8 {
		newTime := time.Now()
		m, _ := time.ParseDuration("-48h")
		LastTime := newTime.Add(m)
		StartTime := LastTime.Format("2006-01-02 15:04:05")
		UserList, _ := models.GetAllUserWithTimeAndAppid(wxInfo.WxAppId, StartTime)
		if len(UserList) > 0 {
			token, _ := GetTokenFromRedis(wxInfo.WxAppId, wxInfo.AppSecret)
			for _, value := range UserList {
				SendImgDo(token, value.Openid, MsgMatterList)
			}
		}
	}
}

// 发送文本提交
func SendMsgDo(token, userOpenId string, content string) {
	SendMsgReqUrl := SengMsgUrl + token
	SendMsg := SendText{}
	fmt.Println("Url->", SendMsgReqUrl)
	SendMsg.MsgType = "text"
	SendMsg.ToUser = userOpenId
	SendMsg.Text.Content = content
	SendJson, _ := json.Marshal(SendMsg)
	res, err := http.Post(SendMsgReqUrl, "POST", strings.NewReader(string(SendJson)))
	_, err = ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("err->", err.Error())
	}
}

// 发送图文提交
func SendImgDo(token, userOpenId string, msgMatter []models.CsWxMsgMatter) {
	SendMsg := SendImg{}
	SendMsgReqUrl := SengMsgUrl + token
	fmt.Println("Url->", SendMsgReqUrl)
	SendMsg.MsgType = "news"
	SendMsg.ToUser = userOpenId
	ImgMsgList := make([]ImgMsgInfo, 0)
	for _, value := range msgMatter {
		imgInfo := ImgMsgInfo{}
		imgInfo.Title = value.Title
		imgInfo.Descrip = value.Descrip
		imgInfo.SkipUrl = value.Url
		imgInfo.PicUrl = "http://" + ResUrl + "/wximg/" + value.Picurl
		ImgMsgList = append(ImgMsgList, imgInfo)
	}
	SendMsg.News.Articles = ImgMsgList
	SendJson, _ := json.Marshal(SendMsg)
	res, err := http.Post(SendMsgReqUrl, "POST", strings.NewReader(string(SendJson)))
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("err->", err.Error())
	}
	SendResInfo := SendRes{}
	err = json.Unmarshal(body, &SendResInfo)
	if err == nil {
		if SendResInfo.Code == 0 {
			fmt.Println("Send model success openud->", userOpenId)
		} else if SendResInfo.Code == 41006 { // Token超时
			fmt.Println("Token Before Refresh ->", AppToken)
			fmt.Println("Token After Refresh->", AppToken)
		} else {
			fmt.Println("Send Default->", SendResInfo.Code, SendResInfo.Msg)
		}
	}
}
